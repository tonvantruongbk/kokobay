﻿function AutoLoadMore(container, temp, btn, val) {
    if (window[temp] === undefined) {
        window[temp] = val;
    }
    var size_li = $(container).size();
    checkLoadMoreCount(container,temp, btn);
    $(container + ':lt(' + size_li + ')').hide();
    $(container + ':lt(' + window[temp] + ')').show();
    $(btn).click(function () {
        window[temp] = (window[temp] + val <= size_li) ? window[temp] + val : size_li;
        $(container + ':lt(' + window[temp] + ')').show();
        checkLoadMoreCount(container, temp, btn);
    });
}

function checkLoadMoreCount(container, temp, btn) {
    var size = $(container).size();
    if (window[temp] >= size) {
        $(btn).hide();
    }
    else {
        $(btn).show();
    }
}