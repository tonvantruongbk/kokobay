$('.single-popup').magnificPopup({ 
  type: 'image',
  removalDelay: 300,
  mainClass: 'mfp-with-fade',
  closeOnContentClick: true
});

$('.gallery').magnificPopup({ 
  
  type: 'image',
  delegate: 'a',
  removalDelay: 300,
  mainClass: 'mfp-with-fade',
  
  gallery:{enabled:true}
  
});
