﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;

namespace KOKOBAY
{

    static public class HtmlExtensions
    {
        public static IHtmlString UploadFor<TSource, TResult>(this HtmlHelper<TSource> html, Expression<Func<TSource, TResult>> propertyExpression)
        {
            var memberName = Reflection.GetPropertyName(propertyExpression);
            return new HtmlString($"<input type=\"file\" name=\"{memberName}\" id=\"{memberName}\" >");
        }
    }

    class Reflection
    {
        public static string GetPropertyName<TSource, TResult>(Expression<Func<TSource, TResult>> propertyExpression)
        {
            return (propertyExpression.Body as MemberExpression).Member.Name;
        }
    }

      
}