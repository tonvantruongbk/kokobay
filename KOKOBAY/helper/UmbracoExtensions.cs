﻿using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace KOKOBAY.helper
{
    /// <summary>
    /// A few useful extension methods for Umbraco IpublishedContent
    /// </summary>
    public static class UmbracoExtensions222
    {
        /// <summary>
        /// Aliases of doctypes that should be excluded from navigation etc.
        /// </summary>
        private readonly static string[] ExcludedAliases = new string[]
        {
            "pageNotFound","tag"
        };
        /// <summary>
        /// Aliases of doctypes that should be excluded from BreadSrumb.
        /// </summary>
        private readonly static string[] ExcludedAliasesBreadSrumb = new string[]
        {
           "newsFolder"
        };
        /// <summary>
        /// Keywords that should be excluded from News Url.
        /// </summary>
        private readonly static string[] ExcludedKeywordUrl = new string[]
        {
           "news/KOKO-news",
           "news/news-board"
        };
        /// <summary>
        /// Gets the home page relative to the current content
        /// </summary>
        /// <param name="content">The current content</param>
        /// <returns>The root node of the site</returns>
        public static IPublishedContent HomePage(this IPublishedContent content)
        {
            return content.AncestorOrSelf(1);
        }
        public static IPublishedContent ChildrenPage(this IPublishedContent content)
        {
            return content.AncestorOrSelf(2);
        }
        public static IPublishedContent C_ChildrenPage(this IPublishedContent content)
        {
            return content.AncestorOrSelf(4);
        }
        /// <summary>
        /// Gets whether this content has a template associated with it
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>Returns true if it has a template otherwise false</returns>
        public static bool HasTemplate(this IPublishedContent content)
        {
            return content.TemplateId > 0;
        }

        /// <summary>
        /// Gets whether this document should be excluded based on it's DocumentTypeAlias
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>True if it should be excluded otherwise false</returns>
        public static bool IsExcluded(this IPublishedContent content)
        {
            return ExcludedAliases.Contains(content.DocumentTypeAlias);
        }

        /// <summary>
        /// Gets whether this document should be excluded based on it's DocumentTypeAlias
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>True if it should be excluded otherwise false</returns>
        public static bool IsExcludedBreadSrumb(this IPublishedContent content)
        {
            return ExcludedAliasesBreadSrumb.Contains(content.DocumentTypeAlias);
        }

        /// <summary>
        /// Gets whether this document should appear in the menu
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>True if it should otherwise false</returns>
        public static bool IsInMenu(this IPublishedContent content)
        {
            return content.HasTemplate() && content.IsVisible() && !content.IsExcluded();
        }

        /// <summary>
        /// Gets whether this document should appear in the bread scrumb
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>True if it should otherwise false</returns>
        public static bool IsInBreadScrumb(this IPublishedContent content)
        {
            return content.IsVisible() && !content.IsExcludedBreadSrumb();
        }

        /// <summary>
        /// Rebuild url for news detail
        /// </summary>
        /// <param name="content">The content to check</param>
        /// <returns>New url string</returns>
        public static string NewsUrl(this IPublishedContent content)
        {
            string newsUrl = content.Url;
            if (string.IsNullOrEmpty(newsUrl)) return string.Empty;

            foreach(var str in ExcludedKeywordUrl)
            {
                newsUrl = newsUrl.Replace(str, "news");
            }
            return newsUrl;
        }
    }
}