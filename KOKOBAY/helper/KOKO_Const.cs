﻿using System;
using System.Reflection;
using System.Web.Compilation;

namespace KOKOBAY.helper
{
    public class KOKO_Const
    {

        public class ConstFormatCustom
        {
            public const string EMAIL =
                @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            public const string CODE_ADDRESS = @"";
            public const string PHONE_NUMBER = @"^(?:\s*)\d{3}-(\d{1})?\d{3}-\d{4}(?:\s*)$"; 
            public const string CODE_LOCALTION = @"^(?:\s*)\d{2}-\d{2}-\d{4}-\d{2}-\d{2}(?:\s*)$"; 
            public const string CODE_FORMAT = @"(^(\d{15})*$"; 
            public const string LOCALTION_FORMAT = @"^(?:\s*)\d{4}-\d{2}-\d{2}(?:\s*)$"; 
                                                                                        
            public const string AN_FORMAT = @"^[0-9a-zA-Z]+$";
            public const string CHARETER = @"[a-zA-Z0-9_~!@#$%^&*()_+-=,.?<>;'|]+$";
            public const string NUMBER = @"^[0-9]*";
        }

        public class ConstFormatDateTime
        {
            public const string DATE_YYYYMMDD = "yyyy/MM/dd";

            public const string DATE_DDMMYYYY = "dd/MM/yyyy";
            public const string DATE_FULL = "yyyy/MM/dd HH:mm";
            public const string DATE_FORMAT_YYYYMMDD = "{0:yyyy/MM/dd}";
            public const string DATE_FORMAT_YYYYMMDD_HHMMSS = "{0:yyyy/MM/dd HH:mm:ss}";
            public const string DATE_FORMAT_YYYYMMDD_HHMM = "{0:yyyy/MM/dd HH:mm}";
            public const string YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
            public const string YYYY_MM_DD_HH_MM_SS = "yyyy/MM/dd HH:mm:ss";
            public const string YYYYMMDD = "yyyyMMdd";
        }
        public class Fields
        {
            public const string TITLE = "title";
            public const string PLACE = "place";
            public const string ISEXPIRED = "isExpired";
            public const string PAGE_TITLE = "pageTitle";
            public const string HERO_TITLE = "heroTitle";
            public const string SUB_HERO_TITLE = "subHeroTitle";
            public const string HERO_IMAGE = "heroImage";
            public const string BODY_CONTENT = "bodyContent";
            public const string BROWSER_TITLE = "browserTitle";
            public const string META_KEYWORDS = "metaKeywords";
            public const string META_DESCRIPTION = "metaDescription";
            public const string META_SHARING_IMAGE = "metaSocialSharingImage";
            public const string FAVICON = "Favicon";
            public const string Iconlangure = "Iconlangure";
            
            public const string SUB_TITLE = "subTitle";
            public const string LOGO = "logo";
            public const string FOOTER = "footer";
            public const string STECHBLOG = "sTechBlog";
            public const string SOCIAL_NETWORK = "socialNetwork";
            public const string NAME = "name";
            public const string LINK = "link";
            public const string CSS_CLASS = "cssClass";
            public const string ICON = "icon";
            public const string ICON_ACTIVE = "iconActive";
            public const string DESCRIPTION = "description";
            public const string PHOTO = "photo";
            public const string PHOTO_DETAIL = "photoDetail";
            public const string IMAGE = "image";
            public const string VIDEO = "video";
            public const string BANNER = "banner";
            public const string DATE = "date";
            public const string BACKGROUND = "background";

            public const string METAKEYWORDS = "metaKeywords";
            public const string METADESCRIPTION = "metaDescription";
            public const string Concept = "concept";
            public const string AboutUs = "aboutUs";
            public const string News = "news";

            //About Us
            public const string AboutTitle = "aboutTitle";
            public const string AboutDescription = "aboutDescription";
            public const string AboutUSTitle = "title";
            public const string AboutUSShortDescription = "shortDescription";

            //Menu Special 
            public const string MenuSpecialTitle = "title";
            public const string MenuSpecialShortDescription = "shortDescription";
            public const string MenuSpecialTextButton = "textButton";
            public const string MenuSpecialUrlDirect = "urlDirect";
            public const string MenuSpecialImage = "image";

            //Cultural
            public const string SliderCulturalHome = "sliderCulturalHome";
            public const string CulturalUrlYoutube = "youtubeLinkShare";

            public const string CulturalTitle = "title";
            public const string CulturalSlogan = "slogan";
            public const string CulturalDescription = "shortDescription";

            //Concept
            public const string ConceptTitle = "title";
            public const string ConceptImage = "image";
            public const string ConceptImageHover = "imageHover";
            public const string ConceptShortDecription = "shortDescription";
            public const string ConceptCategory = "conceptCategoryFolder";
            public const string ConceptItem = "conceptFolder";
            public const string ConceptUrl = "urlConcept";

            //Galary Item
            public const string GalleryName = "galleryName";
            public const string Thumbnail = "thumbnail";
            public const string Gallery = "gallery";
            public const string Caption = "caption";

            //Contact Us
            public const string ContactInfomation = "infomation";
            public const string ContactAddress = "ContactAdress";
            public const string ContactPhone = "ContactPhone";
            public const string ContactEmail = "ContactEmail";
            public const string ContactMessage = "ContactMessage";

            //Trademark Item
            public const string TrademarkTtile = "title";
            public const string TrademarkLogo = "TracemarkLogo";
            public const string TrademarkUrl = "TracemarkUrl";
            public const string TrademarkTimeEstablish = "TracemarkTimeEstablish";

            //Google Map
            public const string DescriptionShop = "descriptionShop";
            public const string GoogleMap = "locationShop";
            //Tab Homepage
            public const string HomePageSlider = "slider";
            public const string HomePageSaleOff = "saleOffHome";
            
            public const string HomePageSlogan = "slogan";
            //Social Networks
            public const string HomePageSocialNetwork = "socialNetwork";
            //Area
            public const string HomePageArealNetwork = "inforContactArea";

            //NewLanding
            public const string NewsImage = "newsLandingImage";
            public const string NewsTitle = "newsLandingTitle";
            public const string NewsShortDescription = "newsLandingShortDescription";
            public const string NewsImageContent = "ImageContent";
            public const string NewsShortDescriptionImageContent = "shortDescriptionImageContent";
            public const string NewsImageSlider = "imageSlider";
            public const string NewsDescription = "newsLandingDescription";
            public const string NewsPublishDate = "newsLandingPublishDate";
            public const string NewsTags = "Tag";
            public const string NewsAuthor = "newsLandingAuthor";

            //Recruitment
            public const string Question = "question";
            public const string Answer = "answer";
            public const string NewsRecruitment = "newsRecruitmentOrther";
            public const string NewsRecruitmentItem = "newsRecruitmentOrtherItem";
            public const string NewsRecruitmentImage = "image";
            public const string NewsRecruitmentTitle = "title";
            public const string NewRecruitmentShortDecription = "shortDescription";
            public const string NewRecruitmentDecription = "description";

            //NewItem
            public const string NewsImageBusiness = "image";
            public const string NewsTitleBusiness = "title";
            public const string NewsShortDescriptionBusiness = "shortDescription";
            public const string NewsDescriptionBusiness = "description";
            public const string NewsPublishDateBusiness = "publishDate";
            public const string NewsTagsBusiness = "tags";
            public const string NewsAuthorBusiness = "author";

            public const string IconGPeople = "iconGPeople";
            //Concept Location
            public const string ContactDescription = "descriptionConcept";
            public const string ContactTownConcept = "townConcept";
            public const string ContactDistrictConcept = "districtConcept";
        }
    }
}
