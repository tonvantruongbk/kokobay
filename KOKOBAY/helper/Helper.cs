﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Reflection;
using System.Text;
using umbraco;
using System.Web.Caching;
using KOKOBAY.Entities;

namespace KOKOBAY.helper
{
    public class Helper
    {
        private static readonly UmbracoHelper UmbracoHelper = new UmbracoHelper(UmbracoContext.Current);
      
        public static ImageItem GetMediaPicker(string imageId)
        {
            try
            {
                var image = new ImageItem();
                var mediaItem = UmbracoHelper.TypedMedia(imageId);
                image.Url = mediaItem.Url;
                image.Alt = mediaItem.Name;
                image.Caption = mediaItem.GetPropertyValue<string>("caption");
                image.Description = mediaItem.GetPropertyValue<string>("description");
                return image;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<ImageItem> GetMultipleMediaPicker(IPublishedContent content, string imageField)
        {
            try
            {
                var listImage = new List<ImageItem>();
                if (!content.HasValue(imageField)) return listImage;

                foreach (var item in content.GetPropertyValue<IEnumerable<IPublishedContent>>(imageField))
                {
                    ImageItem ItemImg = new ImageItem();

                    ItemImg.Url = item.Url;
                    ItemImg.Alt = item.Name;

                    listImage.Add(ItemImg);
                }

                return listImage;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}