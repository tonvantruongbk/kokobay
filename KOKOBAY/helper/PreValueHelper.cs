﻿using System.Collections.Generic;
using System.Linq;
using Examine;
using Examine.SearchCriteria;
using Umbraco.Web;
using System;
using Umbraco.Core.Models;
using KOKOBAY.Models;
using System.Web.Mvc;
using System.Xml.XPath;
using System.Web.Configuration;

namespace KOKOBAY.helper
{
  
    public static class PreValueHelper
    {

        private const string APP_SETTING_ERROR_MESSAGE = "Invalid or missing appSetting, ";

        public static List<SelectListItem> GetPreValuesFromDataTypeId(int dataTypeId)
        {
            List<SelectListItem> preValueSelectorList = new List<SelectListItem>();

            XPathNodeIterator iterator = umbraco.library.GetPreValues(dataTypeId);
            iterator.MoveNext();
            XPathNodeIterator preValues = iterator.Current.SelectChildren("preValue", "");

            while (preValues.MoveNext())
            {
                string preValueIdAsString = preValues.Current.GetAttribute("id", "");
                int preValueId = 0;
                int.TryParse(preValueIdAsString, out preValueId);
                string preValue = preValues.Current.Value;
                preValueSelectorList.Add(new SelectListItem { Value = preValue, Text = preValue });
            }

            return preValueSelectorList;
        }

        public static List<SelectListItem> GetPreValuesFromAppSettingName(string appSettingName)
        {
            int dataTypeId = GetIntFromAppSetting(appSettingName);
            List<SelectListItem> preValues = GetPreValuesFromDataTypeId(dataTypeId);
            return preValues;
        }

        private static int GetIntFromAppSetting(string appSettingName)
        {
            int intValue = 0;
            string setting = GetStringFromAppSetting(appSettingName);
            if (!int.TryParse(setting, out intValue))
            {
                throw new Exception(APP_SETTING_ERROR_MESSAGE + appSettingName);
            }
            return intValue;
        }

        private static string GetStringFromAppSetting(string appSettingName)
        {
            string setting = WebConfigurationManager.AppSettings[appSettingName] as string;
            if (String.IsNullOrEmpty(setting))
            {
                throw new Exception(APP_SETTING_ERROR_MESSAGE + appSettingName);
            }
            return setting;
        }
    }
}