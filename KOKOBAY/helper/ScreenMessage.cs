﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KOKOBAY.helper
{
    public class ScreenMessage
    {
        // Error
        public const string E0001 = "Bạn hãy nhập {0}.";
        public const string E0002 = "{0} không đúng định dạng.";
        public const string E0003=  "{0} hãy nhập không quá {1} kí tự.";
        public const string E0004 = "{0} không đúng định dạng. vd:0988123456";
        public const string E0005 = "{0} hãy nhập không bé hơn {1}.";
        public const string E0006 = "{0} hãy nhập số trong khoảng {1} và {2}.";
    }
}