﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class NewsModel : BaseViewModel
    {

        #region global properties
        public List<NewsItem> LstNews { get; set; }
        public List<NewsItem> LstNewPopular { get; set; }
        public List<NewsItem> LstEvents { get; set; }
        public string ContentNames { get; set; }


        public string LastestNewsSubtitle { get; set; }
        public string LastestNewsTitle { get; set; }
        public string PopularNewsSubtitle { get; set; }
        public string PopularNewsTitle { get; set; }
        public ImageItem BannerImage { get; set; }

        #endregion

        #region global method
        public NewsModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public NewsModel(IPublishedContent content) : base(content)
        {
            getNews(content);
        }
        #endregion


        /// <summary>
        /// Get News
        /// </summary>
        /// <param name="content"></param>
        private void getNews(IPublishedContent content)
        {
            LstNews = new List<NewsItem>();
            LstNewPopular = new List<NewsItem>();
            LstEvents = new List<NewsItem>();
            BannerImage = new ImageItem();
            LastestNewsSubtitle = content.GetPropertyValue<string>("LastestNewsSubtitle");
            LastestNewsTitle = content.GetPropertyValue<string>("LastestNewsTitle");
            PopularNewsSubtitle = content.GetPropertyValue<string>("PopularNewsSubtitle");
            PopularNewsTitle = content.GetPropertyValue<string>("PopularNewsTitle");

            if(content.GetPropertyValue<IPublishedContent>("BannerImage")!=null)
            BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());

            try
            {
                ContentNames = content.Name;
                IEnumerable<IPublishedContent> lstNewContent = content.Children;
                IEnumerable<IPublishedContent> lstNewContentPopular = content.Children.Skip(5);

                foreach (var item in lstNewContent)
                {
                    NewsItem ni = new NewsItem();
                    ni.Image = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetPropertyValue<string>("title");
                    ni.ShortDescription = item.GetPropertyValue<string>("shortDescription");


                    ni.Description = item.GetPropertyValue<string>("description");
                    ni.PublishDate = item.GetPropertyValue<DateTime>("publishDate");
                    ni.Author = item.GetPropertyValue<string>("author");
                    LstNews.Add(ni);
                }
                foreach (var item in lstNewContentPopular)
                {
                    NewsItem ni = new NewsItem();
                    ni.Image = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetPropertyValue<string>("title");
                    ni.ShortDescription = item.GetPropertyValue<string>("shortDescription");
                    ni.Description = item.GetPropertyValue<string>("description");
                    ni.PublishDate = item.GetPropertyValue<DateTime>("publishDate");
                    ni.Author = item.GetPropertyValue<string>("author");
                    LstNewPopular.Add(ni);
                }

                foreach (var item in content.Children)
                {
                    if (item.GetPropertyValue<string>("typeNews") == "Event")
                    {
                        NewsItem ni = new NewsItem();
                        var itemIMG = item.GetProperty("avatarImage");
                        ni.Image = Helper.GetMediaPicker(((IPublishedContent)(item.GetProperty("avatarImage").Value)).Id + "");
                        ni.UrlNews = item.Url;
                        ni.Title = item.GetProperty("title").Value + "";
                        ni.ShortDescription = item.GetProperty("shortDescription").Value + "";
                        LstEvents.Add(ni);
                    }
                }

                if (LstNews.Count() > 0)
                {
                    LstNews = LstNews.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}