﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class PlayModel : BaseViewModel
    {

        #region global properties
        public List<StayItem> LstStay { get; set; }
        public List<EventDetailItem> LstEvent { get; set; }
        public ImageItem BannerImage { get; set; }

        public string Title { get; set; }
        public string SubTitle { get; set; }
        #endregion

        #region global method
        public PlayModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public PlayModel(IPublishedContent content) : base(content)
        {
            getPlay(content);
        }
        #endregion


        /// <summary>
        /// Get News
        /// </summary>
        /// <param name="content"></param>
        private void getPlay(IPublishedContent content)
        {
            LstStay = new List<StayItem>();

            try
            {
                 BannerImage =  Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("bannerImage").Id.ToString());
                Title = content.GetPropertyValue<string>("title");
                SubTitle = content.GetPropertyValue<string>("subTitle");
                foreach (var item in content.Children)
                {
                    StayItem ni = new StayItem();
                    ni.Image = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatar").Id.ToString());
                    ni.LstImage = Helper.GetMultipleMediaPicker(item, "imageSlider");
                    ni.UrlStay = item.Url;
                    ni.Link = item.GetPropertyValue<string>("link");

                    ni.ImageSlider = Helper.GetMultipleMediaPicker(item, "imageSlider");

                    ni.Title = item.GetPropertyValue<string>("title");
                    ni.SubTitle = item.GetPropertyValue<string>("subTitle");
                    ni.ShortDescription = item.GetPropertyValue<string>("shortdescription");
                    ni.PublishDate = item.GetPropertyValue<DateTime>("publishDate");

                    LstStay.Add(ni);
                }

                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var EventNode = umbracoHelper.ContentSingleAtXPath("//event");

                IEnumerable<IPublishedContent> lstEventContent = EventNode.Children;
                LstEvent = new List<EventDetailItem>();
                foreach (var item in lstEventContent)
                {
                    LstEvent.Add(CommonDetailModel.EventDetail(item));
                }

                if (LstStay.Count() > 0)
                {
                    LstStay = LstStay.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}