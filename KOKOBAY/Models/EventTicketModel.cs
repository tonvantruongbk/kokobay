﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class EventTicketModel : BaseViewModel
    {

        #region global properties
        public List<EventTicketDetailItem> LstEventTicket { get; set; }

        public string  TitleEventTicket { get; set; }
        public string SubTitleEventTicket { get; set; }

   
        public ImageItem BannerImage { get; set; }

        #endregion

        #region global method
        public EventTicketModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public EventTicketModel(IPublishedContent content) : base(content)
        {
            getListEventTicket(content);
        }
        #endregion


        /// <summary>
        /// Get News
        /// </summary>
        /// <param name="content"></param>
        private void getListEventTicket(IPublishedContent content)
        {
         
            BannerImage = new ImageItem();
        
            if(content.GetPropertyValue<IPublishedContent>("BannerImage")!=null)
            BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());

            TitleEventTicket = content.GetPropertyValue<string>("TitleEventTicket");
            SubTitleEventTicket= content.GetPropertyValue<string>("SubTitleEventTicket");

            try
            {
                LstEventTicket = new List<EventTicketDetailItem>();
                IEnumerable<IPublishedContent> lstNewContent = content.Children.Take(20);
                foreach (var item in lstNewContent)
                {
                    EventTicketDetailItem ni = new EventTicketDetailItem();
                    ni.AvatarImage = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
                    ni.Title = item.GetPropertyValue<string>("title");
                    ni.SubTitle = item.GetPropertyValue<string>("subTitle");
                    ni.ShortDescription = item.GetPropertyValue<string>("shortDescription");
                    ni.VideoLink = item.Url;
                    ni.LinkBookTicket = item.GetPropertyValue<string>("linkBookTicket");
                    LstEventTicket.Add(ni);

                }
               
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}