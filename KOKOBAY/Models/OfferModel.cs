using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class OfferModel : BaseViewModel
    {

        #region global properties
        public  OfferItem  Current { get; set; }
        public List<OfferDetailItem> lstOffers { get; set; }

        #endregion

        #region global method
        public OfferModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public OfferModel(IPublishedContent content) : base(content)
        {
            Current = new OfferItem();
            Current.BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("bannerImage")); 
            Current.IconBannerImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("iconBannerImage")); 
            Current.Title = content.GetPropertyValue<string>("title");
            Current.SubTitle = content.GetPropertyValue<string>("subTitle");

            lstOffers = new List<OfferDetailItem>();

            try
            { 
                foreach (var item in content.Children)
                {
                    lstOffers.Add(CommonDetailModel.OfferDetail(item));
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }

        }
        #endregion


        
    }
}
