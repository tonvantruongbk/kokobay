using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class FeastModel : BaseViewModel
    {

        #region global properties
        public List<FeastDetailItem> LstFeasts { get; set; }
       
        public string ContentNames { get; set; }
        public string DisplayHome { get; set; }
        public string FeastCallNumber { get; set; }
        public string FeastSubtitle { get; set; }
        public string FeastTitle { get; set; }
        public Boolean DisplayBooking { get; set; }
        public ImageItem FeastImageBanner { get; set; }
        #endregion

        #region global method
        public FeastModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public FeastModel(IPublishedContent content) : base(content)
        {
            getFeast(content);

            DisplayHome = content.GetPropertyValue<string>("DisplayHome");
            FeastCallNumber = content.GetPropertyValue<string>("FeastCallNumber");
            FeastSubtitle = content.GetPropertyValue<string>("FeastSubtitle");
            FeastTitle = content.GetPropertyValue<string>("FeastTitle");
            DisplayBooking = content.GetPropertyValue<Boolean>("displayBooking");
            FeastImageBanner = Helper.GetMediaPicker(content.GetPropertyValue<string>("FeastImageBanner"));

        }
        #endregion


        private void getFeast(IPublishedContent content)
        {
            LstFeasts = new List<FeastDetailItem>();

            try
            {
                ContentNames = content.Name;
                foreach (var item in content.Children)
                {
                    LstFeasts.Add(CommonDetailModel.FeastDetail(item));
                }
                
                if (LstFeasts.Count() > 0)
                {
                    LstFeasts = LstFeasts.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}
