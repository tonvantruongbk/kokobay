﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;
using Newtonsoft.Json;


namespace KOKOBAY.Models
{
    public class PlayDetailModel : BaseViewModel
    {
        #region global properties
        public NewsItem newItem { get; set; }
        public List<NewsItem> LstNews { get; set; }
        #endregion

        #region global method
        public PlayDetailModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public PlayDetailModel(IPublishedContent content) : base(content)
        {
            getDetailNews(content);
            //getListNews(content);
        }
        #endregion

        /// <summary>
        /// Get Detail News
        /// </summary>
        /// <param name="content"></param>
        private void getDetailNews(IPublishedContent content)
        {
            newItem = new NewsItem();
            newItem.Title = content.GetPropertyValue<string>("Title");
            newItem.Description = content.GetPropertyValue<string>("description");
            newItem.PublishDate = content.GetPropertyValue<DateTime>("publishDate");
            newItem.Author = content.GetPropertyValue<string>("author");
            newItem.UrlNews = content.Url;
        }

    }
}