using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class ContactModel : BaseViewModel
    {

        #region global properties
        public string ContentNames { get; set; }

        public ContactItem Current { get; set; }
        #endregion

        #region global method
        public ContactModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public ContactModel(IPublishedContent content) : base(content)
        {
            Current = CommonDetailModel.Contact(content);
        }
        #endregion



    }
}
