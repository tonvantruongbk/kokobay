﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;
using Newtonsoft.Json;


namespace KOKOBAY.Models
{
    public class EventTicketDetailModel : BaseViewModel
    {
        #region global properties
        public EventTicketDetailItem EventTicketDetailItem { get; set; }
        
       public ImageItem BannerImage { get; set; }
        #endregion

        #region global method
        public EventTicketDetailModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public EventTicketDetailModel(IPublishedContent content) : base(content)
        {
            getDetailTicket(content);

        }
        #endregion


        /// <summary>
        /// Get Detail News
        /// </summary>
        /// <param name="content"></param>
        private void getDetailTicket(IPublishedContent content)
        {
            BannerImage = new ImageItem();
            if (content.GetPropertyValue<IPublishedContent>("BannerImage") != null)
                BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());

            EventTicketDetailItem = new EventTicketDetailItem();

            //BannerImage = Helper.GetMediaPicker(content.Parent.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());
            EventTicketDetailItem.Title = content.GetPropertyValue<string>("Title");

            EventTicketDetailItem.SubTitle = content.GetPropertyValue<string>("SubTitle");
            EventTicketDetailItem.ShortDescription = content.GetPropertyValue<string>("ShortDescription");

            EventTicketDetailItem.PHONE = content.GetPropertyValue<string>("PHONE");

            EventTicketDetailItem.HOURSOFOPERATION = content.GetPropertyValue<string>("HOURSOFOPERATION");

            EventTicketDetailItem.AGERESTRICTIONS = content.GetPropertyValue<string>("AGERESTRICTIONS");
            EventTicketDetailItem.PRICERANGE = content.GetPropertyValue<string>("PRICERANGE");
            EventTicketDetailItem.VideoTitle = content.GetPropertyValue<string>("VideoTitle");
            EventTicketDetailItem.VideoSubTitle = content.GetPropertyValue<string>("VideoSubTitle");
            EventTicketDetailItem.VideoDescription = content.GetPropertyValue<string>("VideoDescription");
            EventTicketDetailItem.VideoLink = content.GetPropertyValue<string>("VideoLink");


            EventTicketDetailItem.TitleGallery = content.GetPropertyValue<string>("TitleGallery");
            EventTicketDetailItem.SubTitleGallery = content.GetPropertyValue<string>("SubTitleGallery");


            EventTicketDetailItem.DetailAgendaTitle = content.GetPropertyValue<string>("DetailAgendaTitle");

            EventTicketDetailItem.DEtailedagenda = content.GetPropertyValue<string>("DEtailedagenda");
            EventTicketDetailItem.DetailedAgendaSubtitle = content.GetPropertyValue<string>("DetailedAgendaSubtitle");
            EventTicketDetailItem.EnableDetailAgenda = content.GetPropertyValue<bool>("EnableDetailAgenda");


            EventTicketDetailItem.GalleryImage = Helper.GetMultipleMediaPicker(content, "GalleryImage");

            EventTicketDetailItem.LocationTitle = content.GetPropertyValue<string>("LocationTitle");
            EventTicketDetailItem.LocationImage = new ImageItem();
            if (content.GetPropertyValue<IPublishedContent>("LocationImage") != null)
                EventTicketDetailItem.LocationImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("LocationImage").Id.ToString());

           
            EventTicketDetailItem.LocationDescription = content.GetPropertyValue<string>("LocationDescription");
            EventTicketDetailItem.DETAILEDAGENDA = content.GetPropertyValue<string>("DETAILEDAGENDA");

            EventTicketDetailItem.LinkBookTicket= content.GetPropertyValue<string>("linkBookTicket");

            Random r = new Random(DateTime.Now.Millisecond);
            var i = 0;
            EventTicketDetailItem.EventTicketDetailColumnsList = new List<EventTicketDetailColumns>();
            foreach (var item in content.Children)
            {
                EventTicketDetailItem.EventTicketDetailColumnsList.Add(new EventTicketDetailColumns());
                EventTicketDetailItem.EventTicketDetailColumnsList[i].EventColumnName = item.GetPropertyValue<string>("EventColumnName");

                var t = item.GetPropertyValue<IEnumerable<IPublishedContent>>("EventTicketDetailRow");

                EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList = new List<EventTicketDetailTimes>();
                var j = 0;
                foreach (var item1 in t)
                {
                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList.Add(new EventTicketDetailTimes());
                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList[j].EventTicketDetailName = item1.GetPropertyValue<string>("EventTicketDetailName");
                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList[j].EventTicketDetailDescription = item1.GetPropertyValue<string>("EventTicketDetailDescription");
                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList[j].FromTime = item1.GetPropertyValue<DateTime>("FromTime");
                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList[j].ToTime = item1.GetPropertyValue<DateTime>("ToTime");

                    EventTicketDetailItem.EventTicketDetailColumnsList[i].EventTicketDetailRowList[j].eventColor = "event-" +  r.Next(1, 5);
                    j++;
                }


                i++;
            }



        }

    }
}