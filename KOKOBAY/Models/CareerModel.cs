using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using System.Web.Mvc;

namespace KOKOBAY.Models
{
    public class CareerModel : BaseViewModel
    {

        #region global properties
        public List<CareerDetailItem> LstCareers { get; set; }
        public List<CareerDetailItem> LstUrgentJobs { get; set; }

        public string CompanyEventSubtitle { get; set; }
        public string CompanyEventTitle { get; set; }
        public bool DisplayMenuTop { get; set; }
        public string HrNewsSubtitle { get; set; }
        public string HrNewsTitle { get; set; }
        public string LastestJobsTitle { get; set; }
        public string Subtitle { get; set; }
        public string Title { get; set; }
        public string UrgentJobsTitle { get; set; }

        public string ContentNames { get; set; }
        public List<SelectListItem> ListDepartment { get; set; }
        public List<SelectListItem> ListProperty { get; set; }
        public List<NewsItem> lstHRNews { get; set; }
        public List<NewsItem> lstEvents { get; set; }

        public ImageItem BannerImage { get; set; }
        #endregion

        #region global method
        public CareerModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public CareerModel(IPublishedContent content) : base(content)
        {
            getCareer(content);

            CompanyEventSubtitle = content.GetPropertyValue<string>("CompanyEventSubtitle");
            CompanyEventTitle = content.GetPropertyValue<string>("CompanyEventTitle");
            DisplayMenuTop = content.GetPropertyValue<bool>("DisplayMenuTop");
            HrNewsSubtitle = content.GetPropertyValue<string>("HrNewsSubtitle");
            HrNewsTitle = content.GetPropertyValue<string>("HrNewsTitle");
            LastestJobsTitle = content.GetPropertyValue<string>("LastestJobsTitle");
            Subtitle = content.GetPropertyValue<string>("Subtitle");
            Title = content.GetPropertyValue<string>("Title");
            UrgentJobsTitle = content.GetPropertyValue<string>("UrgentJobsTitle");

            ListDepartment = PreValueHelper.GetPreValuesFromAppSettingName("DepartmentsDropdownID");
            ListProperty = PreValueHelper.GetPreValuesFromAppSettingName("PropertiesDropdownID");



            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var NewsNode = umbracoHelper.ContentSingleAtXPath("//news");

            IEnumerable<IPublishedContent> lstNewContent = NewsNode.Children;
            lstHRNews = new List<NewsItem>();
            lstEvents = new List<NewsItem>();
            foreach (var item in lstNewContent)
            {
                if (item.GetPropertyValue<string>("typeNews") == "Event")
                {
                    NewsItem ni = new NewsItem();
                    var itemIMG = item.GetProperty("avatarImage");
                    ni.Image = Helper.GetMediaPicker(((IPublishedContent)(item.GetProperty("avatarImage").Value)).Id + "");
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetProperty("title").Value + "";
                    ni.ShortDescription = item.GetProperty("shortDescription").Value + "";
                    ni.Description = ((Umbraco.Web.Models.DynamicPublishedContent)NewsNode).Url;
                    lstEvents.Add(ni);
                }
                else if (item.GetPropertyValue<string>("typeNews") == "HRNew")
                {
                    NewsItem ni = new NewsItem();
                    var itemIMG = item.GetProperty("avatarImage");
                    ni.Image = Helper.GetMediaPicker(((IPublishedContent)(item.GetProperty("avatarImage").Value)).Id + "");
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetProperty("title").Value + "";
                    ni.ShortDescription = item.GetProperty("shortDescription").Value + "";
                    ni.Description = ((Umbraco.Web.Models.DynamicPublishedContent)NewsNode).Url;
                    lstHRNews.Add(ni);
                }
            }

            BannerImage = new ImageItem();
            if (content.GetPropertyValue<IPublishedContent>("BannerImage") != null)
                BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());
        }
        #endregion


        private void getCareer(IPublishedContent content)
        {
            LstCareers = new List<CareerDetailItem>();
            LstUrgentJobs = new List<CareerDetailItem>();

            try
            {
                ContentNames = content.Name;
                foreach (var item in content.Children)
                {
                    LstCareers.Add(CommonDetailModel.CareerDetail(item));
                }
                foreach (var item in content.Children)
                {
                    if (item.GetPropertyValue<bool>("JobUrgent") == true)
                    {
                        LstUrgentJobs.Add(CommonDetailModel.CareerDetail(item));
                    }
                }

                if (LstCareers.Count() > 0)
                {
                    LstCareers = LstCareers.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}
