using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class ProjectModel : BaseViewModel
    {

        #region global properties
        public List<ProjectDetailItem> LstProjects { get; set; }
        public List<ProjectDetailItem> LstProjectsLastest { get; set; }
        public string ContentNames { get; set; } 
        #endregion

        #region global method
        public ProjectModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public ProjectModel(IPublishedContent content) : base(content)
        {
            getProject(content);
             
        }
        #endregion


        private void getProject(IPublishedContent content)
        {
            LstProjects = new List<ProjectDetailItem>();
            LstProjectsLastest = new List<ProjectDetailItem>();

            try
            {
                ContentNames = content.Name;
                foreach (var item in content.Children.Take(5))
                {
                    LstProjects.Add(CommonDetailModel.ProjectDetail(item));
                }
                foreach (var item in content.Children.Skip(5).Take(5))
                {
                    LstProjectsLastest.Add(CommonDetailModel.ProjectDetail(item));
                }

                if (LstProjects.Count() > 0)
                {
                    LstProjects = LstProjects.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}
