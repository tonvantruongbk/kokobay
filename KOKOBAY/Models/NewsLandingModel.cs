﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;
using Newtonsoft.Json;


namespace KOKOBAY.Models
{
    public class NewsLandingModel : BaseViewModel
    {
        #region global properties
        public NewsItem newItem { get; set; }
        public List<NewsItem> LstNews { get; set; }
        public ImageItem BannerImage { get; set; }
        #endregion

        #region global method
        public NewsLandingModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public NewsLandingModel(IPublishedContent content) : base(content)
        {
            getDetailNews(content);
            getListNews(content);
        }
        #endregion


        /// <summary>
        /// Get Detail News
        /// </summary>
        /// <param name="content"></param>
        private void getDetailNews(IPublishedContent content)
        {
            newItem = new NewsItem();
            BannerImage = new ImageItem();
            newItem.Title = content.GetPropertyValue<string>("Title");
            newItem.Image = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
            newItem.Description = content.GetPropertyValue<string>("description");
            newItem.PublishDate = content.GetPropertyValue<DateTime>("publishDate");
            newItem.Author = content.GetPropertyValue<string>("author");
            newItem.UrlNews = content.Url;

            if (content.Parent.GetPropertyValue<IPublishedContent>("BannerImage") != null)
                BannerImage = Helper.GetMediaPicker(content.Parent.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());
        }

        public void getListNews(IPublishedContent content)
        {
            LstNews = new List<NewsItem>();
            if (content.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags) != null)
            {
                string[] tag = content.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags).ToString().Split(',');
                var countTag = tag.Count();
                var news = content.Parent;
                foreach (var item in news.Children.Where(x => x.Id != content.Id))
                {
                    if (item.GetPropertyValue<string>("tag") != null)
                    {
                        string[] tagItem = item.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags).ToString().Split(',');
                        var check = false;
                        var tagsItemCount = tagItem.Count();
                        if (tagsItemCount > 0)
                        {
                            for (var i = 0; i < tagsItemCount; i++)
                            {
                                for (var j = 0; j < countTag; j++)
                                {
                                    if (tagItem[i].Contains(tag[j]))
                                    {
                                        check = true;
                                    }
                                }
                            }
                        }
                        if (check)
                        {
                            NewsItem nItem = new NewsItem();
                            nItem.Title = item.GetPropertyValue<string>("Title");
                            nItem.Image = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
                            nItem.ShortDescription= item.GetPropertyValue<string>("shortDescription");
                            nItem.PublishDate = item.GetPropertyValue<DateTime>("publishDate");
                            nItem.Author = item.GetPropertyValue<string>("author");
                            nItem.UrlNews = item.Url;
                            LstNews.Add(nItem);
                        }
                    }
                }
            }
        }
    }
}