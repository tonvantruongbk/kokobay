﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;
using Newtonsoft.Json;


namespace KOKOBAY.Models
{
    public class PolicyItemModel : BaseViewModel
    {
        #region global properties
        public NewsItem newItem { get; set; }
        public ImageItem bannerImage { get; set; }
        #endregion

        #region global method
        public PolicyItemModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public PolicyItemModel(IPublishedContent content) : base(content)
        {
            getDetailNews(content);
        
        }
        #endregion


        /// <summary>
        /// Get Detail News
        /// </summary>
        /// <param name="content"></param>
        private void getDetailNews(IPublishedContent content)
        {
            newItem = new NewsItem();
            newItem.Title = content.GetPropertyValue<string>("Title");
            newItem.Description = content.GetPropertyValue<string>("description");

            bannerImage = new ImageItem();
            bannerImage = Helper.GetMediaPicker(content.Parent.GetPropertyValue<string>("bannerImage"));
        }

    }
}