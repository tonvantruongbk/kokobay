using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;

namespace KOKOBAY.Models
{
    public class CareerDetailModel : BaseViewModel
    {
        #region global properties
        public CareerDetailItem CurrentCareerDetail { get; set; }
        public List<CareerDetailItem> LstCareer { get; set; }

        public ImageItem BannerImage { get; set; }
        #endregion

        #region global method
        public CareerDetailModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public CareerDetailModel(IPublishedContent content) : base(content)
        {
            getDetailCareer(content);
            BannerImage = new ImageItem();

               if (content.Parent.GetPropertyValue<IPublishedContent>("BannerImage") != null)
                BannerImage = Helper.GetMediaPicker(content.Parent.GetPropertyValue<IPublishedContent>("BannerImage").Id.ToString());
        }
        #endregion
 
        private void getDetailCareer(IPublishedContent content)
        {
            CurrentCareerDetail = CommonDetailModel.CareerDetail(content);

            LstCareer = new List<CareerDetailItem>();
            foreach (var item in content.Parent.Children.Where(a=>a.GetPropertyValue<string>("JobTitle")!= CurrentCareerDetail.JobTitle).Take(3))
            {
                LstCareer.Add(CommonDetailModel.CareerDetail(item));
            }
        }
 
    }
}
