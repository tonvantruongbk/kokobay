﻿using KOKOBAY.Entities;
using KOKOBAY.helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public static class CommonDetailModel
    {
        public static ProjectDetailItem ProjectDetail(IPublishedContent content)
        {
            var item = new ProjectDetailItem();
            item.BenefitsDescription = content.GetPropertyValue<IEnumerable<IPublishedContent>>("BenefitsDescription");
            item.BenefitsNote = content.GetPropertyValue<string>("BenefitsNote");
            item.BenefitsSubtitle = content.GetPropertyValue<string>("BenefitsSubtitle");
            item.BenefitTitle = content.GetPropertyValue<string>("BenefitTitle");
            item.FacilitiesImages = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FacilitiesImages");
            item.FacilitiesSubtitle = content.GetPropertyValue<string>("FacilitiesSubtitle");
            item.FacilitiesTitle = content.GetPropertyValue<string>("FacilitiesTitle");
            item.InteriorImage = content.GetPropertyValue<IEnumerable<IPublishedContent>>("InteriorImage");
            item.InteriorSubtitle = content.GetPropertyValue<string>("InteriorSubtitle");
            item.InteriorTitle = content.GetPropertyValue<string>("InteriorTitle");
            item.LocationDescription = content.GetPropertyValue<string>("LocationDescription");
            item.LocationDescriptionMore = content.GetPropertyValue<string>("LocationDescriptionMore");
            item.LocationImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("LocationImage"));
            item.LocationSubtitle = content.GetPropertyValue<string>("LocationSubtitle");
            item.LocationTitle = content.GetPropertyValue<string>("LocationTitle");
            item.LocationTitle2 = content.GetPropertyValue<string>("LocationTitle2");
            item.MasterPlanImage = content.GetPropertyValue<IEnumerable<IPublishedContent>>("MasterPlanImage");
            item.MasterPlanSubtitle = content.GetPropertyValue<string>("MasterPlanSubtitle");
            item.MasterPlanTitle = content.GetPropertyValue<string>("MasterPlanTitle");
            item.ProjectDescription = content.GetPropertyValue<string>("ProjectDescription");
            item.ProjectDescriptionMore = content.GetPropertyValue<string>("ProjectDescriptionMore");
            item.ProjectImageSlider = content.GetPropertyValue<IEnumerable<IPublishedContent>>("ProjectImageSlider");
            item.ProjectInvestorText = content.GetPropertyValue<string>("ProjectInvestorText");
            item.ProjectInvestorTitle = content.GetPropertyValue<string>("ProjectInvestorTitle");
            item.ProjectOrganizationText = content.GetPropertyValue<string>("ProjectOrganizationText");
            item.ProjectOrganizationTitle = content.GetPropertyValue<string>("ProjectOrganizationTitle");
            item.ProjectRoomsText = content.GetPropertyValue<string>("ProjectRoomsText");
            item.ProjectRoomsTitle = content.GetPropertyValue<string>("ProjectRoomsTitle");
            item.ProjectScaleText = content.GetPropertyValue<string>("ProjectScaleText");
            item.ProjectScaleTitle = content.GetPropertyValue<string>("ProjectScaleTitle");
            item.ProjectSubtitle = content.GetPropertyValue<string>("ProjectSubtitle");
            item.ProjectTitle = content.GetPropertyValue<string>("ProjectTitle");


            item.EnableBenefits = content.GetPropertyValue<bool>("EnableBenefits");
            item.EnableFacilities = content.GetPropertyValue<bool>("EnableFacilities");
            item.EnableInteriorDesign = content.GetPropertyValue<bool>("EnableInteriorDesign");
            item.EnableLocation = content.GetPropertyValue<bool>("EnableLocation");
            item.EnableMasterPlan = content.GetPropertyValue<bool>("EnableMasterPlan");


            item.ContactUsSubtitle = content.GetPropertyValue<string>("contactUsSubtitle");
            item.ContactUsTitle = content.GetPropertyValue<string>("ContactUsTitle");


            item.ProjectURL = content.Url;

            return item;
        }
        public static FeastDetailItem FeastDetail(IPublishedContent content)
        {
            var item = new FeastDetailItem();
            item.FeastDetailCallNumber = content.GetPropertyValue<string>("FeastDetailCallNumber");
            item.FeastDetailGalleryPhotos = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FeastDetailGalleryPhotos");
            item.FeastDetailGalleryTitle = content.GetPropertyValue<string>("FeastDetailGalleryTitle");
            item.FeastDetailGalleryVideos = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FeastDetailGalleryVideos");
            item.FeastDetailHeaderShortDescription = content.GetPropertyValue<string>("FeastDetailHeaderShortDescription");
            item.FeastDetailHeaderTitle = content.GetPropertyValue<string>("FeastDetailHeaderTitle");
            item.FeastDetailImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("FeastDetailImage"));
            item.FeastDetailImagesGallery = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FeastDetailImagesGallery");
            item.FeastDetailMenuDescription = content.GetPropertyValue<string>("FeastDetailMenuDescription");
            item.FeastUrlRedirect = content.GetPropertyValue<string>("FeastUrlRedirect");
            
            item.FeastDetailMenuGallery = Helper.GetMultipleMediaPicker(content, "FeastDetailMenuGallery");
            item.FeastDetailMenuTitle = content.GetPropertyValue<string>("FeastDetailMenuTitle");
            item.FeastDetailSubtitle = content.GetPropertyValue<string>("FeastDetailSubtitle");
            item.FeastDetailTestimonials = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FeastDetailTestimonials");
            item.FeastDetailTitle = content.GetPropertyValue<string>("FeastDetailTitle");


            item.FeastListDescription = content.GetPropertyValue<string>("FeastListDescription");
            item.FeastListImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("FeastListImage"));
            item.FeastListSubtile = content.GetPropertyValue<string>("FeastListSubtile");
            item.FeastListTitle = content.GetPropertyValue<string>("FeastListTitle");
            item.FeastIntroImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("FeastIntroImage")); 
            item.FeastIntroTitle = content.GetPropertyValue<string>("FeastIntroTitle");
            item.FeastIntroSubTitle = content.GetPropertyValue<string>("FeastIntroSubTitle");

            item.FeastURL = content.Url;

            return item;
        }

        public static AboutUsItem AboutUs(IPublishedContent content)
        {
            var item = new AboutUsItem();
            item.AchievementImages = Helper.GetMediaPicker(content.GetPropertyValue<string>("AchievementImages"));
            item.AchievementSubtitle = content.GetPropertyValue<string>("AchievementSubtitle");
            item.AchievementTitle = content.GetPropertyValue<string>("AchievementTitle");
            item.DisplayMenuTop = content.GetPropertyValue<bool>("DisplayMenuTop");
            item.HeaderImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("HeaderImage"));
            item.HeartDescription = content.GetPropertyValue<string>("HeartDescription");
            item.HearthImages = Helper.GetMediaPicker(content.GetPropertyValue<string>("HearthImages"));
            item.HeartSubtitle = content.GetPropertyValue<string>("HeartSubtitle");
            item.HeartSubtitle2 = content.GetPropertyValue<string>("HeartSubtitle2");
            item.HeartTitle = content.GetPropertyValue<string>("HeartTitle");
            item.LeadingEntertainmentDescription = content.GetPropertyValue<string>("LeadingEntertainmentDescription");
            item.LeadingEntertainmentDescriptionMore = content.GetPropertyValue<string>("LeadingEntertainmentDescriptionMore");
            item.LeadingEntertainmentNote = content.GetPropertyValue<string>("LeadingEntertainmentNote");
            item.LeadingEntertainmentTitle = content.GetPropertyValue<string>("LeadingEntertainmentTitle");
            item.LeadingEntertainmentVideo = content.GetPropertyValue<string>("LeadingEntertainmentVideo");
            item.ProjectsImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("ProjectsImage"));
            item.ProjectsSubtitle = content.GetPropertyValue<string>("ProjectsSubtitle");
            item.ProjectsTitle = content.GetPropertyValue<string>("ProjectsTitle");
            item.Testimonial = content.GetPropertyValue<IEnumerable<IPublishedContent>>("Testimonial");

            item.UniqueLocationImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("UniqueLocationImage"));
            item.UniqueLocationDescription = content.GetPropertyValue<string>("UniqueLocationDescription");
            item.UniqueLocationEastText = content.GetPropertyValue<string>("UniqueLocationEastText");
            item.UniqueLocationEastTitle = content.GetPropertyValue<string>("UniqueLocationEastTitle");
            item.UniqueLocationNorthText = content.GetPropertyValue<string>("UniqueLocationNorthText");
            item.UniqueLocationNorthTitle = content.GetPropertyValue<string>("UniqueLocationNorthTitle");
            item.UniqueLocationSouthText = content.GetPropertyValue<string>("UniqueLocationSouthText");
            item.UniqueLocationSouthTitle = content.GetPropertyValue<string>("UniqueLocationSouthTitle");
            item.UniqueLocationTitle = content.GetPropertyValue<string>("UniqueLocationTitle");
            item.UniqueLocationWestText = content.GetPropertyValue<string>("UniqueLocationWestText");
            item.UniqueLocationWestTitle = content.GetPropertyValue<string>("UniqueLocationWestTitle");
            item.EnableProjectsList = content.GetPropertyValue<bool>("EnableProjectsList");

            return item;
        }

        public static ContactItem Contact(IPublishedContent content)
        {
            var item = new ContactItem();
            item.ContactHeaderBanner = Helper.GetMediaPicker(content.GetPropertyValue<string>("ContactHeaderBanner"));

            item.ContactHeaderBannerText = Helper.GetMediaPicker(content.GetPropertyValue<string>("ContactHeaderBannerText"));
            item.ContactHeaderBannerLogo = Helper.GetMediaPicker(content.GetPropertyValue<string>("ContactHeaderBannerLogo"));
            

            item.ContactImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("ContactImage"));
            item.ContactOffices = content.GetPropertyValue<IEnumerable<IPublishedContent>>("ContactOffices");
            item.ContactSubtitle = content.GetPropertyValue<string>("ContactSubtitle");
            item.contactFooterAddress = content.GetPropertyValue<string>("contactFooterAddress");
            item.ContactTitle = content.GetPropertyValue<string>("ContactTitle");
            item.DisplayMenuTop = content.GetPropertyValue<bool>("DisplayMenuTop");

            return item;
        }

        public static ShoppingItem Shopping(IPublishedContent content)
        {
            var item = new ShoppingItem();
            item.BannerList = Helper.GetMultipleMediaPicker(content, "sliderImageBanner");
            item.DisplayHome = content.GetPropertyValue<string>("DisplayHome");
            item.FunctionDescription = content.GetPropertyValue<string>("FunctionDescription");

            item.FunctionDescriptionMore = content.GetPropertyValue<string>("FunctionDescriptionMore");
            item.FunctionImages = content.GetPropertyValue<IEnumerable<IPublishedContent>>("FunctionImages");
            item.FunctionTitle = content.GetPropertyValue<string>("FunctionTitle");
            item.GiftShopDescription = content.GetPropertyValue<string>("GiftShopDescription");

            item.GiftShopDescriptionMore = content.GetPropertyValue<string>("GiftShopDescriptionMore");
            item.GiftShopDescription2 = content.GetPropertyValue<string>("GiftShopDescription2");
            item.GiftShopGalery = content.GetPropertyValue<IEnumerable<IPublishedContent>>("GiftShopGalery");
            item.GiftShopTitle = content.GetPropertyValue<string>("GiftShopTitle");
            item.GiftShopTitle2 = content.GetPropertyValue<string>("GiftShopTitle2");
            item.Subtitle = content.GetPropertyValue<string>("Subtitle");
            item.Title = content.GetPropertyValue<string>("Title");

            return item;
        }

        public static OfferItem Offer(IPublishedContent content)
        {
            var item = new OfferItem();
            item.DisplayMenuTop = content.GetPropertyValue<bool>("DisplayMenuTop");
            item.ImageGalerry = content.GetPropertyValue<IEnumerable<IPublishedContent>>("ImageGalerry");

            return item;
        }

        public static OfferDetailItem OfferDetail(IPublishedContent content)
        {
            var item = new OfferDetailItem();
            item.Author = content.GetPropertyValue<string>("Author");
            item.AvatarImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("AvatarImage"));
            item.Description = content.GetPropertyValue<string>("Description");
            item.DisplayHome = content.GetPropertyValue<bool>("DisplayHome");
            item.OfferType = content.GetPropertyValue<string>("OfferType");
            item.PublishDate = content.GetPropertyValue<DateTime>("PublishDate");
            item.ShortDescription = content.GetPropertyValue<string>("ShortDescription");
            item.Tag = content.GetPropertyValue<IEnumerable<string>>("Tag");
            item.Title = content.GetPropertyValue<string>("Title");

            item.UrlItem = content.Url;

            return item;
        }

        public static CareerDetailItem CareerDetail(IPublishedContent content)
        {
            var item = new CareerDetailItem();
            item.JobCategory = content.GetPropertyValue<string>("JobCategory");
            item.JobDescriptionDetail = content.GetPropertyValue<string>("JobDescriptionDetail");
            item.JobDescriptionTitle = content.GetPropertyValue<string>("JobDescriptionTitle");
            item.JobDescriptionTitle2 = content.GetPropertyValue<string>("JobDescriptionTitle2");
            item.JobLevel = content.GetPropertyValue<string>("JobLevel");
            item.JobLocation = content.GetPropertyValue<string>("JobLocation");
            item.JobPurposeDetail = content.GetPropertyValue<string>("JobPurposeDetail");
            item.JobPurposeTitle = content.GetPropertyValue<string>("JobPurposeTitle");
            item.JobResponsibilitiesDetail = content.GetPropertyValue<string>("JobResponsibilitiesDetail");
            item.JobResponsibilitiesTitle = content.GetPropertyValue<string>("JobResponsibilitiesTitle");
            item.JobSpecificDetail = content.GetPropertyValue<string>("JobSpecificDetail");
            item.JobSpecificTitle = content.GetPropertyValue<string>("JobSpecificTitle");
            item.JobTitle = content.GetPropertyValue<string>("JobTitle");
            item.PerformanceRequirementsDetail = content.GetPropertyValue<string>("PerformanceRequirementsDetail");
            item.PerformanceRequirementsTitle = content.GetPropertyValue<string>("PerformanceRequirementsTitle");
            item.PostDate = content.GetPropertyValue<DateTime>("PostDate");
            item.Preferred = content.GetPropertyValue<string>("Preferred");
            item.Type = content.GetPropertyValue<string>("Type");
            item.WhatWeCanOfferDescription = content.GetPropertyValue<string>("WhatWeCanOfferDescription");
            item.WhatWeCanOfferTitle = content.GetPropertyValue<string>("WhatWeCanOfferTitle");
            item.JobDepartment = content.GetPropertyValue<string>("JobDepartment");
            item.ChooseProperties = content.GetPropertyValue<string>("ChooseProperties");
            item.JobUrgent = content.GetPropertyValue<bool>("JobUrgent");

            item.Url = content.Url;
            return item;
        }

        public static EventDetailItem EventDetail(IPublishedContent content)
        {
            var item = new EventDetailItem();
            item.Author = content.GetPropertyValue<string>("Author");
            item.AvatarImage = Helper.GetMediaPicker(content.GetPropertyValue<string>("AvatarImage"));
            item.DateFrom = content.GetPropertyValue<DateTime>("DateFrom");
            item.DateTo = content.GetPropertyValue<DateTime>("DateTo");
            item.Description = content.GetPropertyValue<string>("Description");
            item.DisplayHome = content.GetPropertyValue<bool>("DisplayHome");
            item.ShortDescription = content.GetPropertyValue<string>("ShortDescription");
            item.Tag = content.GetPropertyValue<IEnumerable<string>>("Tag");
            item.Title = content.GetPropertyValue<string>("Title");
            item.UrlEvent = content.Url;

            return item;
        }

    }
}