using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;

namespace KOKOBAY.Models
{
    public class ProjectDetailModel : BaseViewModel
    {
        #region global properties
        public ProjectDetailItem CurrentProjectDetail { get; set; }
        public List<ProjectDetailItem> LstProject { get; set; }
        #endregion

        #region global method
        public ProjectDetailModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public ProjectDetailModel(IPublishedContent content) : base(content)
        {
            getDetailProject(content);
        }
        #endregion
 
        private void getDetailProject(IPublishedContent content)
        {
            CurrentProjectDetail = CommonDetailModel.ProjectDetail(content);
            LstProject = new List<ProjectDetailItem>();
            foreach (var item in content.Parent.Children.Take(6))
            {
                LstProject.Add(CommonDetailModel.ProjectDetail(item));
            }
        }
         
    }
}
