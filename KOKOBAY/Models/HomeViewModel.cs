﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;

using umbraco.NodeFactory;
using System.Reflection;
using KOKOBAY.Entities;

namespace KOKOBAY.Models
{
    public class HomeViewModel : BaseViewModel
    {
        #region global properties
        //[KOKOBAY] 
        public string KOKOTitle { get; set; }
        public string KOKOKeyWord { get; set; }
        public string KOKODescription { get; set; }

        public string TitleOffer { get; set; }
        public string SubTitleOffer { get; set; }

        public ImageItem BannerImage { get; set; }
        public IEnumerable<IPublishedContent> ImageHomeoffers { get; set; }

        public string UrlNews { get; set; }

        public List<ImageItem> LstImageHomeoffers { get; set; }

        public ImageItem HomeImageBackGround { get; set; }
 
        public string AppTitle { get; set; }
        public string AppSubTitle { get; set; }
        public string AppShortDescription { get; set; }
        public string AppLinkIOS { get; set; }
        public string AppLinkAndroid { get; set; }
       
        //About Us
        public string AboutMetaTitle { get; set; }
        public string AboutMetaKeyWord { get; set; }
        public string AboutMetaDescription { get; set; }
        public string AboutTitle { get; set; }
        public string AboutUrl { get; set; }
        public string AboutDescription { get; set; }
        public ImageItem ImageAboutUs { get; set; }
        public string AboutUsDescription { get; set; }
        public Boolean DisplayInHome { get; set; }

        ////Concept Menu Special
        //public string MenuSpecialTitle { get; set; }
        //public string MenuSpecialShortDescription { get; set; }
        //public string MenuSpecialUrl { get; set; }
        //public string MenuSpecialTextButton { get; set; }
        //public ImageItem MenuSpecialImage { get; set; }

        //Recruitment
        public string RecruitmentTitle { get; set; }
        public string RecruitmentUrl { get; set; }
        public ImageItem RecruitmentImage { get; set; }

        //Social
        public string TitleSocial { get; set; }
        public string UrlSocial { get; set; }
        public ImageItem ImageVer { get; set; }
        public ImageItem ImageHor { get; set; }
        public string SocialDescription { get; set; }
        //Cultural
        public string UrlCultural { get; set; }
        public List<ImageItem> LstCulturalImage { get; set; }

        public string UrlYoutubeCultural { get; set; }
        //Concept
        public string ConceptMetaTitle { get; set; }
        public string ConceptMetaKeyWord { get; set; }
        public string ConceptMetaDescription { get; set; }
        public string ConceptTitle { get; set; }
        public string ConceptDescription { get; set; }
        //public List<ConceptItem> LstConcept { get; set; }
      
        //public List<CategoryConceptItem> LstConceptCategory { get; set; }
        //public List<ConceptItem> LstConceptItem { get; set; }
        //public List<ConceptMenuSpecialItem> LstConceptMenuSpecialItem { get; set; }
        //News
        public List<NewsItem> LstNews { get; set; }
        //Gallery
        public List<GalleryItem> LstGallery { get; set; }
        //Contact Us
        public string ContactInfo { get; set; }
        // 
        public string TradeMarkTitle { get; set; }
        //public List<TrademarkItem> LstTMItem { get; set; }
        //Google Map
        public List<string> titleLoca { get; set; }
        public List<string> desLoca { get; set; }
        public List<decimal> latLoca { get; set; }
        public List<decimal> logLoca { get; set; }
        //public List<SliderHomePageItem> LstSlider { get; set; }
        //public List<SloganHomePageItem> LstSlogan { get; set; }
        public List<ImageItem> LstCulturalSlider { get; set; }

        public Boolean DisplayInHomeProject { get; set; }
        public List<ProjectDetailItem> LstProjects { get; set; }

        #endregion

        #region global method
        public HomeViewModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public HomeViewModel(IPublishedContent content) : base(content)
        {
            getKOKOInfo(content);
            getKOKONews(content);
            getProjects(content);
        }
        #endregion

        #region private method


        private void getProjects(IPublishedContent content)
        {
            try
            {
                //var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                //var NewsNode = umbracoHelper.ContentSingleAtXPath("//projects");
                //IEnumerable <IPublishedContent> lstNewContent = ((IPublishedContent)content.Children.Where(c => c.DocumentTypeAlias == "projects")).Children;

                foreach(var itemMenu in content.Children)
                {

                    if (itemMenu.DocumentTypeAlias == "projects")
                    {
                        DisplayInHomeProject = itemMenu.GetPropertyValue<Boolean>("displayInHome");
                        LstProjects = new List<ProjectDetailItem>();
                        foreach (var item in itemMenu.Children.Take(6))
                        {
                            LstProjects.Add(CommonDetailModel.ProjectDetail(item));
                        }
                    }
                }
              
            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }



        /// <summary>
        /// Get Info KOKO Content
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOInfo(IPublishedContent content)
        {
            try
            {
                BannerImage = new ImageItem();
                KOKOTitle = content.GetPropertyValue<string>(KOKO_Const.Fields.TITLE);
                KOKOKeyWord = content.GetPropertyValue<string>(KOKO_Const.Fields.METAKEYWORDS);
                KOKODescription = content.GetPropertyValue<string>(KOKO_Const.Fields.METADESCRIPTION);

         
        TitleOffer = content.GetPropertyValue<string>("titleOffers");
                SubTitleOffer = content.GetPropertyValue<string>("subTitleOffers");

        ImageHomeoffers = Content.GetPropertyValue<IEnumerable<IPublishedContent>>("imageHomeOffers");


                HomeImageBackGround = Helper.GetMediaPicker(Content.GetPropertyValue<IPublishedContent>("homeImageBackGround").Id.ToString());
                AppTitle = content.GetPropertyValue<string>("appTitle");
                AppSubTitle = content.GetPropertyValue<string>("appSubTitle");
                AppShortDescription = content.GetPropertyValue<string>("appShortDescription");
                AppLinkIOS = content.GetPropertyValue<string>("appLinkIOS");
                AppLinkAndroid = content.GetPropertyValue<string>("appLinkAndroid");
                DisplayInHome= content.GetPropertyValue<Boolean>("displayInHome");
                if (Content.GetPropertyValue<IPublishedContent>("bannerImage")!=null)
                { 
                 BannerImage = Helper.GetMediaPicker(Content.GetPropertyValue<IPublishedContent>("bannerImage").Id.ToString());
                }
                LstImageHomeoffers = new List<ImageItem>();
                foreach (var item in ImageHomeoffers)
                {

                    ImageItem img = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("Image").Id.ToString());

                    LstImageHomeoffers.Add(img);
                }


            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }

        /// <summary>
        /// Get About Us 
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOAbountUs(IPublishedContent content)
        {
            try
            {
                if (content.Children.Count() > 0)
                {
                    var AboutUs = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("aboutUs"));
                    AboutUrl = AboutUs.Url;
                    AboutTitle = AboutUs.GetPropertyValue<string>(KOKO_Const.Fields.AboutTitle);
                    AboutDescription = AboutUs.GetPropertyValue<string>(KOKO_Const.Fields.AboutDescription);
                    string MetaKeyWord = AboutUs.GetPropertyValue<string>("metaKeywords");
                    ImageAboutUs = Helper.GetMediaPicker(AboutUs.GetPropertyValue<string>("image"));
                    AboutUsDescription = AboutUs.GetPropertyValue<string>("aboutHomeDescription");
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
        /// <summary>
        /// Get About Us 
        /// </summary>
        /// <param name="content"></param>
        private void getConceptMenuSpecial(IPublishedContent content)
        {
            //try
            //{
            //    LstConceptMenuSpecialItem = new List<ConceptMenuSpecialItem>();
            //    if (content.Children.Count() > 0)
            //    {
            //        var ConceptMenuSpecial = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("conceptMenuSpecial"));
            //        foreach (var ConceptMenuSpecialItem in ConceptMenuSpecial.Children)
            //        {
            //            ConceptMenuSpecialItem Item = new ConceptMenuSpecialItem();
            //            Item.MenuSpecialTitle = ConceptMenuSpecialItem.GetPropertyValue<string>(KOKO_Const.Fields.MenuSpecialTitle);
            //            Item.MenuSpecialShortDescription = ConceptMenuSpecialItem.GetPropertyValue<string>(KOKO_Const.Fields.MenuSpecialShortDescription);
            //            Item.MenuSpecialTextButton = ConceptMenuSpecialItem.GetPropertyValue<string>(KOKO_Const.Fields.MenuSpecialTextButton);
            //            Item.MenuSpecialUrlDirect = ConceptMenuSpecialItem.GetPropertyValue<string>(KOKO_Const.Fields.MenuSpecialUrlDirect);
            //            Item.MenuSpecialImage = Helper.GetMediaPicker(ConceptMenuSpecialItem.GetPropertyValue<string>(KOKO_Const.Fields.MenuSpecialImage));
            //            LstConceptMenuSpecialItem.Add(Item);
            //        }
            //    }

            //    var ContentCultural= content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("cultural"));
            //    UrlYoutubeCultural = ContentCultural.GetPropertyValue<string>(KOKO_Const.Fields.CulturalUrlYoutube) + "";
            //    //UrlYoutubeCultural =  content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("conceptMenuSpecial"));
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            //}
        }
        /// <summary>
        /// Get About Us 
        /// </summary>
        /// <param name="content"></param>
        private void getRecruitment(IPublishedContent content)
        {
            try
            {
                var Recruitment = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("recruitment"));

                if (Recruitment != null)
                {
                    RecruitmentUrl = Recruitment.Url;
                    RecruitmentTitle = Recruitment.GetPropertyValue<string>("slogan");
                    RecruitmentImage = Helper.GetMediaPicker(Recruitment.GetPropertyValue<string>("imageHome"));
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }

        /// <summary>
        /// Get Info KOKO Content
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOConcept(IPublishedContent content)
        {
            //LstConceptCategory = new List<CategoryConceptItem>();
            //try
            //{
            //    Node nodeCategoryContent = new Node(2576);
            //    foreach (var item in nodeCategoryContent.ChildrenAsList)
            //    {
            //        CategoryConceptItem CCItem = new CategoryConceptItem();
            //        CCItem.Title = item.Name;
            //        CCItem.ID = item.Id;
            //        LstConceptCategory.Add(CCItem);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            //}
        }


        /// <summary>
        /// Get News
        /// </summary>
        /// <param name="content"></param>
        private void getKOKONews(IPublishedContent content)
        {
            LstNews = new List<NewsItem>();
            try
            {
                var NewsContent = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("news"));
                UrlNews = NewsContent.Url;
                IEnumerable<IPublishedContent> lstNewContent = NewsContent.Children.Take(5);

                foreach (var item in lstNewContent)
                {
                    NewsItem ni = new NewsItem();
                    ni.Image = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>("avatarImage").Id.ToString());
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetPropertyValue<string>("title");
                    ni.ShortDescription = item.GetPropertyValue<string>("shortDescription");


                    ni.Description = item.GetPropertyValue<string>("description");
                    ni.PublishDate = item.GetPropertyValue<DateTime>("publishDate");
                    ni.Author = item.GetPropertyValue<string>("author");
                    LstNews.Add(ni);
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }

        /// <summary>
        /// Get Gallery
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOSocial(IPublishedContent content)
        {
            try
            {
                var Social = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("socialResponsibility"));
                if (Social != null)
                {
                    TitleSocial = Social.GetPropertyValue<string>("title");
                    UrlSocial = Social.Url;
                    ImageVer = Helper.GetMediaPicker(Social.GetPropertyValue<string>("imageVertical"));
                    ImageHor = Helper.GetMediaPicker(Social.GetPropertyValue<string>("imageHorizontal"));
                    SocialDescription = Social.GetPropertyValue<string>("shortDescription");
                }

            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }


        /// <summary>
        /// Get Gallery
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOCultural(IPublishedContent content)
        {
            //try
            //{

            //    var Cultural = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("cultural"));
            //    if (Cultural != null)
            //    {
            //        LstCulturalImage = new List<ImageItem>();
            //        var slider = Cultural.GetPropertyValue<ArchetypeModel>(KOKO_Const.Fields.SliderCulturalHome);
            //        foreach (var item in slider)
            //        {
            //            ImageItem sh = new ImageItem();
            //            sh = Helper.GetMediaPicker(item.GetValue("image"));
            //            LstCulturalImage.Add(sh);
            //        }
            //        UrlCultural = Cultural.Url;
            //    }

            //}
            //catch (Exception ex)
            //{
            //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            //}
        }
        /// <summary>
        /// Get Contact us
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOContactUs(IPublishedContent content)
        {
            try
            {
                var ContactUs = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("contactUs"));
                if (ContactUs != null)
                    ContactInfo = ContactUs.GetPropertyValue<string>(KOKO_Const.Fields.ContactInfomation);

            }
            catch (Exception ex)
            {
                LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }

        /// <summary>
        /// Get Info KOKO Content
        /// </summary>
        /// <param name="content"></param>
        private void getKOKOFooter(IPublishedContent content)
        {
            //LstTMItem = new List<TrademarkItem>();
            //try
            //{
            //    var TradeMark = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("trademarks"));
            //    if (TradeMark != null)
            //    {
            //        TradeMarkTitle = TradeMark.GetPropertyValue<string>(KOKO_Const.Fields.TrademarkTtile);
            //        foreach (var item in TradeMark.Children)
            //        {
            //            TrademarkItem ti = new TrademarkItem();
            //            ti.Logo = Helper.GetMediaPicker(item.GetPropertyValue<string>(KOKO_Const.Fields.TrademarkLogo));
            //            ti.Url = item.GetPropertyValue<string>(KOKO_Const.Fields.TrademarkUrl);
            //            ti.TimeEstablish = item.GetPropertyValue<DateTime>(KOKO_Const.Fields.TrademarkTimeEstablish);
            //            LstTMItem.Add(ti);
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
        }
    }


    //private void getKOKOGoogleMap(IPublishedContent content)
    //{
    //    //titleLoca = new List<string>();
    //    //desLoca = new List<string>();
    //    //latLoca = new List<decimal>();
    //    //logLoca = new List<decimal>();
    //    //try
    //    //{
    //    //    var Location = content.Children.FirstOrDefault(x => x.DocumentTypeAlias.Equals("shopPosition"));
    //    //    foreach (var item in Location.Children)
    //    //    {
    //    //        titleLoca.Add(item.Name);
    //    //        desLoca.Add(item.GetPropertyValue<string>(KOKO_Const.Fields.DescriptionShop));
    //    //        var map = item.GetPropertyValue<AngularGoogleMaps.Model>(KOKO_Const.Fields.GoogleMap);
    //    //        latLoca.Add(map.Latitude);
    //    //        logLoca.Add(map.Longitude);
    //    //    }

    //    //}
    //    //catch (Exception ex)
    //    //{
    //    //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
    //    //}
    //}


    ///// <summary>
    ///// Get Slider
    ///// </summary>
    ///// <param name="content"></param>
    //private void getKOKOSlider(IPublishedContent content)
    //{
    //    //LstSlider = new List<SliderHomePageItem>();
    //    //try
    //    //{
    //    //    var slider = content.GetPropertyValue<ArchetypeModel>(KOKO_Const.Fields.HomePageSlider);
    //    //    foreach (var item in slider)
    //    //    {
    //    //        SliderHomePageItem sh = new SliderHomePageItem();
    //    //        sh.Title = item.GetValue("title");
    //    //        sh.ShortDescription= item.GetValue("shortDescription");
    //    //        sh.Url = item.GetValue("url");
    //    //        sh.Image = Helper.GetMediaPicker(item.GetValue("image"));

    //    //        LstSlider.Add(sh);
    //    //    }
    //    //}
    //    //catch (Exception ex)
    //    //{
    //    //    LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
    //    //}
    //}

    ///// <summary>
    ///// Get Slogan
    ///// </summary>
    ///// <param name="content"></param>
    //private void getKOKOSlogan(IPublishedContent content)
    //{
    //    LstSlogan = new List<SloganHomePageItem>();
    //    try
    //    {
    //        var slogans = content.GetPropertyValue<ArchetypeModel>(KOKO_Const.Fields.HomePageSlogan);
    //        foreach (var item in slogans)
    //        {
    //            SloganHomePageItem slogan = new SloganHomePageItem();
    //            slogan.Title = item.GetValue("title");
    //            if (Helper.GetMediaPicker(item.GetValue("image")) == null)
    //            {
    //                ImageItem img = new ImageItem();
    //                img.Url = "Content\\images\\icon1.png";
    //                img.Alt = ". . .";
    //                img.Description = ". . .";
    //                slogan.Image = img;
    //            }
    //            else
    //            {
    //                slogan.Image = Helper.GetMediaPicker(item.GetValue("image"));
    //            }
    //            slogan.ShortDescreiption = item.GetValue("shortDescription");
    //            LstSlogan.Add(slogan);
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
    //    }
    //}

    //public List<ConceptItem> getKOKOGetListConceptItem(int IDcategory)
    //{

    //    LstConceptItem = new List<ConceptItem>();
    //    try
    //    {
    //        Node nodeContent = new Node(2577);
    //        foreach (var itemConcept in nodeContent.ChildrenAsList)
    //        {
    //            var x = itemConcept.GetProperty("conceptCategory");
    //            if (x.ToString().Equals(IDcategory.ToString()))
    //            {
    //                ConceptItem ci = new ConceptItem();
    //                ci.Title = itemConcept.GetProperty(KOKO_Const.Fields.ConceptTitle) + "";
    //                ci.Image = Helper.GetMediaPicker((itemConcept.GetProperty(KOKO_Const.Fields.ConceptImage) + ""));
    //                ci.ImageHover = Helper.GetMediaPicker(itemConcept.GetProperty(KOKO_Const.Fields.ConceptImageHover) + "");
    //                ci.Url = itemConcept.GetProperty(KOKO_Const.Fields.ConceptUrl) + "";
    //                LstConceptItem.Add(ci);
    //            }
    //        }

    //    }
    //    catch (Exception ex)
    //    {
    //        LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
    //    }
    //    return LstConceptItem;
    //}

    #endregion
    //}
}