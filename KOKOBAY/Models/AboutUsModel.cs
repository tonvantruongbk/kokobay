using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class AboutUsModel : BaseViewModel
    {

        #region global properties
        public AboutUsItem Current { get; set; }
        public List<ProjectDetailItem> LstProjects { get; set; }
        #endregion

        #region global method
        public AboutUsModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public AboutUsModel(IPublishedContent content) : base(content)
        {
            Current = CommonDetailModel.AboutUs(content);

            var NodLangure = content.Parent;
            foreach (var itemMenu in NodLangure.Children)
            {
               
                    if (itemMenu.DocumentTypeAlias == "projects")
                    {
                        LstProjects = new List<ProjectDetailItem>();
                        foreach (var item in itemMenu.Children.Take(6))
                        {
                            LstProjects.Add(CommonDetailModel.ProjectDetail(item));
                        }
                    }
               
            }

        }
        #endregion
        
    }
}
