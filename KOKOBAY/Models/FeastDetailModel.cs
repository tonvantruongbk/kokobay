using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;
using Umbraco.Core.Logging;
using System.Globalization;

namespace KOKOBAY.Models
{
    public class FeastDetailModel : BaseViewModel
    {
        #region global properties
        public FeastDetailItem CurrentFeastDetail { get; set; }
        public List<FeastDetailItem> LstFeast { get; set; }
        #endregion

        #region global method
        public FeastDetailModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public FeastDetailModel(IPublishedContent content) : base(content)
        {
            getDetailFeast(content);
            getListFeast(content);
        }
        #endregion
 
        private void getDetailFeast(IPublishedContent content)
        {
            CurrentFeastDetail = CommonDetailModel.FeastDetail(content);
        }

        public void getListFeast(IPublishedContent content)
        {
            LstFeast = new List<FeastDetailItem>();
            if (content.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags) != null)
            {
                string[] tag = content.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags).ToString().Split(',');
                var countTag = tag.Count();
                var news = content.Parent;
                foreach (var item in news.Children.Where(x => x.Id != content.Id))
                {
                    if (item.GetPropertyValue<string>("tag") != null)
                    {
                        string[] tagItem = item.GetPropertyValue<string>(KOKO_Const.Fields.NewsTags).ToString().Split(',');
                        var check = false;
                        var tagsItemCount = tagItem.Count();
                        if (tagsItemCount > 0)
                        {
                            for (var i = 0; i < tagsItemCount; i++)
                            {
                                for (var j = 0; j < countTag; j++)
                                {
                                    if (tagItem[i].Contains(tag[j]))
                                    {
                                        check = true;
                                    }
                                }
                            }
                        }
                        if (check)
                        {
                            LstFeast.Add(CommonDetailModel.FeastDetail(item));
                        }
                    }
                }
            }
        }
    }
}
