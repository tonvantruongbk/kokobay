﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class StayModel : BaseViewModel
    {

        #region global properties
        public List<StayItem> LstStay { get; set; }
        public ImageItem BannerImage { get; set; }
        public string Title { get; set; }
        public string SubTitle {get;set;}
        #endregion

        #region global method
        public StayModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public StayModel(IPublishedContent content) : base(content)
        {
            getNews(content);
        }
        #endregion


        /// <summary>
        /// Get News
        /// </summary>
        /// <param name="content"></param>
        private void getNews(IPublishedContent content)
        {
            LstStay = new List<StayItem>();
       
            
            try
            {
                BannerImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>("bannerImage").Id.ToString());
                Title = content.GetPropertyValue<string>("title");
                SubTitle = content.GetPropertyValue<string>("subTitle");

                foreach (var item in content.Children)
                {
                    foreach (var itemCh in item.Children)
                    {
                        StayItem ni = new StayItem();
                        ni.Image = Helper.GetMediaPicker(itemCh.GetPropertyValue<IPublishedContent>("Image").Id.ToString());
                        ni.UrlStay = itemCh.GetPropertyValue<string>("urlRedirect");
                        ni.UrlBooking = itemCh.GetPropertyValue<string>("urlBooking");
                        ni.Title = itemCh.GetPropertyValue<string>("title");
                        ni.SubTitle = itemCh.GetPropertyValue<string>("subTitle");
                        ni.ShortDescription = itemCh.GetPropertyValue<string>("description");
                        ni.PublishDate = itemCh.GetPropertyValue<DateTime>("publishDate");

                        LstStay.Add(ni);
                    }
                }
               

                if (LstStay.Count() > 0)
                {
                    LstStay = LstStay.OrderByDescending(s => s.PublishDate).ToList();
                }
            }
            catch (Exception ex)
            {
                //LogHelper.Error(MethodBase.GetCurrentMethod().DeclaringType, ex.Message, ex);
            }
        }
    }
}