using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using KOKOBAY.helper;
using Umbraco.Web;

namespace KOKOBAY.Models
{
    public class ShoppingModel : BaseViewModel
    {

        #region global properties
        public ShoppingItem Current { get; set; }
        #endregion

        #region global method
        public ShoppingModel(IPublishedContent content, CultureInfo culture)
         : base(content, culture)
        {
        }
        public ShoppingModel(IPublishedContent content) : base(content)
        {
            Current = CommonDetailModel.Shopping(content);
        }
        #endregion

    }
}
