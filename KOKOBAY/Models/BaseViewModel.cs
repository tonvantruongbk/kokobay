﻿using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Models;
using KOKOBAY.helper;
using System.Globalization;
using Umbraco.Web;


namespace KOKOBAY.Models
{
    public class BaseViewModel : RenderModel
    {
        #region Global Method
        public BaseViewModel(IPublishedContent content, CultureInfo culture)
            : base(content, culture)
        {
        }

        public BaseViewModel(IPublishedContent content)
            : base(content)
        {
            // Get page infor
            BrowserTitle = content.GetPropertyValue<string>(KOKO_Const.Fields.BROWSER_TITLE) + "";


            if (content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.META_SHARING_IMAGE) != null)
                MetaSocialSharingImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.META_SHARING_IMAGE).Id.ToString());

            MetaKeywords = content.GetPropertyValue<string>(KOKO_Const.Fields.META_KEYWORDS) + "";
            MetaDescription = content.GetPropertyValue<string>(KOKO_Const.Fields.META_DESCRIPTION) + "";

            if (content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.META_SHARING_IMAGE) != null)
                MetaSocialSharingImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.META_SHARING_IMAGE).Id.ToString());
            PageTitle = content.GetPropertyValue<string>(KOKO_Const.Fields.PAGE_TITLE) + "";


            if (PageTitle == "")
                PageTitle = content.GetPropertyValue<string>("Title") + "";

            if (PageTitle == "")
                PageTitle = BrowserTitle;


            HeroTitle = content.GetPropertyValue<string>(KOKO_Const.Fields.HERO_TITLE) + "";
            HeroSubTitle = content.GetPropertyValue<string>(KOKO_Const.Fields.SUB_HERO_TITLE) + "";
            if (content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.HERO_IMAGE) != null)
                HeroImage = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.HERO_IMAGE).Id.ToString());
            if (content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.FAVICON) != null)
                Favicon = Helper.GetMediaPicker(content.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.FAVICON).Id.ToString());

            Menu = new List<MenuItem>();
            var home = content.HomePage();




            //public string PhoneNumber { get; set; }
            //public string SocialUrl { get; set; }
            //public string SocialIconByCss { get; set; }

            PhoneNumber = home.GetPropertyValue<string>("PhoneNumber");
            FooterAddress = home.GetPropertyValue<string>("FooterAddess");
            SocialNetwork = home.GetPropertyValue<IEnumerable<IPublishedContent>>("SocialNetwork");

            var pageChildren = content.ChildrenPage();
            if (home.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.LOGO) != null)
                Logo = Helper.GetMediaPicker(home.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.LOGO).Id.ToString());


            MenuChildrenLeft = new List<MenuChildrenItem>();
            MenuChildrenRight = new List<MenuChildrenItem>();
            MenuChildrenTop = new List<MenuChildrenItem>();
            MenuChildLevel2 = new List<MenuChildrenItem>();
            MenuChildLevel3 = new List<MenuChildrenItem>();
            int dem = 0;

            //foreach (var item in home.Children)
            //{

            if (dem == 0)
                Patents = pageChildren.GetPropertyValue<IEnumerable<IPublishedContent>>("Patents");


            enablePatents = pageChildren.GetPropertyValue<bool>("enablePatents");
            foreach (var item in home.Children)
            {
                if (dem == 0)
                    Patents = item.GetPropertyValue<IEnumerable<IPublishedContent>>("Patents");

                MenuItem MenuIT = new MenuItem();
                MenuIT.Name = item.GetPropertyValue<string>("languageNotation");
                MenuIT.NodeName = item.Name;
                MenuIT.Url = item.UrlAbsolute();
                MenuIT.Iconlangure = new ImageItem();

                if (item.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.Iconlangure) != null)
                    MenuIT.Iconlangure = Helper.GetMediaPicker(item.GetPropertyValue<IPublishedContent>(KOKO_Const.Fields.Iconlangure).Id.ToString());
                Menu.Add(MenuIT);
                dem = dem + 1;
            }
            //}
            LstPolicy = new List<MenuItem>();
            foreach (var item in pageChildren.Children)
            {
                //policy
                if (item.DocumentTypeAlias == "policy")
                {
                    foreach (var policyItem in item.Children)
                    {
                        MenuItem ItemPlolicy = new MenuItem();
                        ItemPlolicy.Name = policyItem.Name;
                        ItemPlolicy.Url = policyItem.Url;
                        LstPolicy.Add(ItemPlolicy);
                    }

                }

                if (item.GetPropertyValue<string>("DisplayHome") == "Left")
                {
                    var menuChildrenItem = new MenuChildrenItem();
                    menuChildrenItem.Name = item.Name;
                    menuChildrenItem.Url = item.Url;
                    MenuChildrenLeft.Add(menuChildrenItem);
                }

                if (item.GetPropertyValue<string>("DisplayHome") == "Right")
                {
                    var menuChildrenItem = new MenuChildrenItem();
                    menuChildrenItem.Name = item.Name;
                    if (item.DocumentTypeAlias != "projects")
                    {
                        menuChildrenItem.Url = item.Url;
                    }
                    MenuChildrenRight.Add(menuChildrenItem);
                }
                if (item.GetPropertyValue<bool>("DisplayMenuTop") == true)
                {

                    var menuChildrenItem = new MenuChildrenItem();
                    menuChildrenItem.Name = item.Name;
                    menuChildrenItem.Url = item.Url;
                    MenuChildrenTop.Add(menuChildrenItem);
                }
                if (item.GetPropertyValue<bool>("DisplayMenuLevel2") == true)
                {
                    foreach (var i in item.Children)
                    {

                        var menuChil2 = new MenuChildrenItem();
                        menuChil2.ParentName = item.Name;
                     
                        if (item.DocumentTypeAlias == "stay")
                        {
                            menuChil2.Name = i.Name;
                            menuChil2.Url = "";
                            var en = i.GetPropertyValue<bool>("enableStayItem");
                            if (en == true)
                            {     //menuChil2.Url = i.GetPropertyValue<string>("UrlRedirect");

                                MenuChildLevel2.Add(menuChil2);
                                foreach (var j in i.Children)
                                {
                                    var menuChil3 = new MenuChildrenItem();
                                    menuChil3.ParentName = i.Name;
                                    menuChil3.Name = j.Name;
                                    menuChil3.Url = j.GetPropertyValue<string>("UrlRedirect");
                                    MenuChildLevel3.Add(menuChil3);
                                }
                            }
                            else
                            {
                                menuChil2.Url = i.Url;
                            }
                        }
                        else
                        {
                            menuChil2.Name = i.Name;
                            menuChil2.Url = i.Url;
                            MenuChildLevel2.Add(menuChil2);
                        }
                    }
                }


            }
            GoogleAnalytics = home.GetPropertyValue<string>("googleAnalytics");
            FacebookPixcel = home.GetPropertyValue<string>("facebookPixcel");

            DisplayMenuRight = home.GetPropertyValue<Boolean>("displayMenuRight");
        }

        #endregion
        #region Properties

        public string Email { get; set; }
        //Meta data
        public string BrowserTitle { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public ImageItem MetaSocialSharingImage { get; set; }
        public ImageItem Favicon { get; set; }

        public string PhoneNumber { get; set; }
        public string FooterAddress { get; set; }

        public IEnumerable<IPublishedContent> SocialNetwork { get; set; }
        //Hero Content
        public string PageTitle { get; set; }
        public string HeroTitle { get; set; }
        public string HeroSubTitle { get; set; }
        public ImageItem HeroImage { get; set; }

        //Common
        public ImageItem Logo { get; set; }
        public string Footer { get; set; }

        public List<MenuItem> Menu { get; set; }
        public List<MenuChildrenItem> MenuChildrenLeft { get; set; }
        public List<MenuChildrenItem> MenuChildrenRight { get; set; }

        public List<MenuChildrenItem> MenuChildrenTop { get; set; }
        public List<MenuChildrenItem> MenuChildLevel2 { get; set; }
        public List<MenuChildrenItem> MenuChildLevel3 { get; set; }

        public List<LinkItem> Social { get; set; }
        public string ShareImage { get; set; }
        public IEnumerable<IPublishedContent> Patents { get; set; }
        public bool enablePatents { get; set; }

        public string GoogleAnalytics { get; set; }
        public string FacebookPixcel { get; set; }
        public bool DisplayMenuRight { get; set; }

        public List<MenuItem> LstPolicy { get; set; }

        #endregion Properties
    }
}