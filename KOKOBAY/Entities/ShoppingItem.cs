using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class ShoppingItem
    {

        public List<ImageItem> BannerList { get; set; }
        public string DisplayHome { get; set; }
        public string FunctionDescription { get; set; }

        public string FunctionDescriptionMore { get; set; }
        public IEnumerable<IPublishedContent> FunctionImages { get; set; }
        public string FunctionTitle { get; set; }
        public string GiftShopDescription { get; set; }

        public string GiftShopDescriptionMore { get; set; }
        public string GiftShopDescription2 { get; set; }
        public IEnumerable<IPublishedContent> GiftShopGalery { get; set; }
        public string GiftShopTitle { get; set; }
        public string GiftShopTitle2 { get; set; }
        public string Subtitle { get; set; }
        public string Title { get; set; }


        public DateTime PublishDate { get; set; }
    }
}
