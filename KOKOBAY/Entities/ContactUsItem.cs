﻿using KOKOBAY.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static KOKOBAY.helper.KOKO_Const;

namespace KOKOBAY.Entities
{
    public class ContactUsItem
    {
        [Display(Name = "Email")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        [StringLength(200, ErrorMessage = ScreenMessage.E0003)]
        [RegularExpression(ConstFormatCustom.EMAIL, ErrorMessage = ScreenMessage.E0002)]

        public string Email { get; set; }

        public string NameCustomer { get; set; }
        public string PhoneNumber { get; set; }

        public string Note { get; set; }
        public string ErrorMessage { get; set; }
    }
}