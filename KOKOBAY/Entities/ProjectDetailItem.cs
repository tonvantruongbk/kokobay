using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class ProjectDetailItem
    {

        public IEnumerable<IPublishedContent> BenefitsDescription { get; set; }
        public string BenefitsNote { get; set; }
        public string BenefitsSubtitle { get; set; }
        public string BenefitTitle { get; set; }
        public IEnumerable<IPublishedContent> FacilitiesImages { get; set; }
        public string FacilitiesSubtitle { get; set; }
        public string FacilitiesTitle { get; set; }
        public IEnumerable<IPublishedContent> InteriorImage { get; set; }
        public string InteriorSubtitle { get; set; }
        public string InteriorTitle { get; set; }
        public string LocationDescription { get; set; }
        public string LocationDescriptionMore { get; set; }
        public ImageItem LocationImage { get; set; }
        public string LocationSubtitle { get; set; }
        public string LocationTitle { get; set; }
        public string LocationTitle2 { get; set; }
        public IEnumerable<IPublishedContent> MasterPlanImage { get; set; }
        public string MasterPlanSubtitle { get; set; }
        public string MasterPlanTitle { get; set; }
        public string ProjectDescription { get; set; }
        public string ProjectDescriptionMore { get; set; }
        public IEnumerable<IPublishedContent> ProjectImageSlider { get; set; }
        public string ProjectInvestorText { get; set; }
        public string ProjectInvestorTitle { get; set; }
        public string ProjectOrganizationText { get; set; }
        public string ProjectOrganizationTitle { get; set; }
        public string ProjectRoomsText { get; set; }
        public string ProjectRoomsTitle { get; set; }
        public string ProjectScaleText { get; set; }
        public string ProjectScaleTitle { get; set; }
        public string ProjectSubtitle { get; set; }
        public string ProjectTitle { get; set; }
        public bool EnableBenefits { get; set; }
        public bool EnableFacilities { get; set; }
        public bool EnableInteriorDesign { get; set; }
        public bool EnableLocation { get; set; }
        public bool EnableMasterPlan { get; set; }


        public string ContactUsSubtitle { get; set; }
        public string ContactUsTitle { get; set; }

        public string ProjectURL { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
