﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KOKOBAY.Entities
{
    public class Error404
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}