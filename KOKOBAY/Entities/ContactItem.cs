using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class ContactItem
    {

        public ImageItem ContactHeaderBanner { get; set; }
        public ImageItem ContactHeaderBannerLogo { get; set; }
        public ImageItem ContactHeaderBannerText { get; set; }

        public ImageItem ContactImage { get; set; }
        public IEnumerable<IPublishedContent> ContactOffices { get; set; }
        public string contactFooterAddress { get; set; }
        public string ContactSubtitle { get; set; }
        public string ContactTitle { get; set; }
        public bool DisplayMenuTop { get; set; }


        public DateTime PublishDate { get; set; }
    }
}
