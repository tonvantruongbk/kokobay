﻿using System.Collections.Generic;
namespace KOKOBAY.Entities
{
    public class MenuItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NodeName { get; set; }
        public string Url { get; set; }
        public bool DisplayMenu { get; set; }
        public ImageItem Iconlangure { get; set; }
    }
    public class MenuChildrenItem
    {
        public string ParentName { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }

    }
}