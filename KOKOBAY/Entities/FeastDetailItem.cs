using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class FeastDetailItem
    {

        public string FeastDetailCallNumber { get; set; }
        public IEnumerable<IPublishedContent> FeastDetailGalleryPhotos { get; set; }
        public string FeastDetailGalleryTitle { get; set; }
        public IEnumerable<IPublishedContent> FeastDetailGalleryVideos { get; set; }
        public string FeastDetailHeaderShortDescription { get; set; }
        public string FeastDetailHeaderTitle { get; set; }
        public ImageItem FeastDetailImage { get; set; }
        public IEnumerable<IPublishedContent> FeastDetailImagesGallery { get; set; }
        public string FeastDetailMenuDescription { get; set; }
        public string FeastUrlRedirect { get; set; }
        public List<ImageItem> FeastDetailMenuGallery { get; set; }
        public string FeastDetailMenuTitle { get; set; }
        public string FeastDetailSubtitle { get; set; }
        public IEnumerable<IPublishedContent> FeastDetailTestimonials { get; set; }
        public string FeastDetailTitle { get; set; }

        public string FeastListDescription { get; set; }
        public ImageItem FeastListImage { get; set; }
        public string FeastListSubtile { get; set; }
        public string FeastListTitle { get; set; }
        public ImageItem FeastIntroImage { get; set; }
        public string FeastIntroTitle { get; set; }
        public string FeastIntroSubTitle { get; set; }

        public string FeastURL { get; set; }

        public DateTime PublishDate { get; set; }
    }
}
