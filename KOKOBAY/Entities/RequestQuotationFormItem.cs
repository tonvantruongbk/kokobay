using System;
using System.Collections.Generic;
using System.Web;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class RequestQuotationFormItem
    {
        public string CompanyName { get; set; }
        public string NumberOfDelegates { get; set; }
        public DateTime EventDate { get; set; }
        public string CheckIn { get; set; }
        public string CheckOut { get; set; }
        public string Nationality { get; set; }
        public string SetUpStyles { get; set; }
        public string Request { get; set; }
        public string PackageRequest { get; set; }
        public string SpecialRequest { get; set; }

    }
}
