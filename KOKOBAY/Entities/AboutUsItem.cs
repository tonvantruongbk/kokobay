﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class AboutUsItem
    {

        public ImageItem AchievementImages { get; set; }
        public string AchievementSubtitle { get; set; }
        public string AchievementTitle { get; set; }
        public bool DisplayMenuTop { get; set; }
        public ImageItem HeaderImage { get; set; }
        public string HeartDescription { get; set; }
        public ImageItem HearthImages { get; set; }
        public string HeartSubtitle { get; set; }
        public string HeartSubtitle2 { get; set; }
        public string HeartTitle { get; set; }
        public string LeadingEntertainmentDescription { get; set; }
        public string LeadingEntertainmentDescriptionMore { get; set; }
        public string LeadingEntertainmentNote { get; set; }
        public string LeadingEntertainmentTitle { get; set; }
        public string LeadingEntertainmentVideo { get; set; }
        public ImageItem ProjectsImage { get; set; }
        public string ProjectsSubtitle { get; set; }
        public string ProjectsTitle { get; set; }
        public IEnumerable<IPublishedContent> Testimonial { get; set; }

        public ImageItem UniqueLocationImage { get; set; }
        public string UniqueLocationDescription { get; set; }
        public string UniqueLocationEastText { get; set; }
        public string UniqueLocationEastTitle { get; set; }
        public string UniqueLocationNorthText { get; set; }
        public string UniqueLocationNorthTitle { get; set; }
        public string UniqueLocationSouthText { get; set; }
        public string UniqueLocationSouthTitle { get; set; }
        public string UniqueLocationTitle { get; set; }
        public string UniqueLocationWestText { get; set; }
        public string UniqueLocationWestTitle { get; set; }


        public bool EnableProjectsList { get; set; }
        public DateTime PublishDate { get; set; }
    }
}