﻿namespace KOKOBAY.Entities
{
    public class ImageItem
    {
        public string Alt { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public string Caption { get; set; }
    }
}