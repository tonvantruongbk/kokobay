using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class CareerDetailItem
    {

        public string JobCategory { get; set; }
        public string JobDescriptionDetail { get; set; }
        public string JobDescriptionTitle { get; set; }
        public string JobDescriptionTitle2 { get; set; }
        public string JobLevel { get; set; }
        public string JobLocation { get; set; }
        public string JobPurposeDetail { get; set; }
        public string JobPurposeTitle { get; set; }
        public string JobResponsibilitiesDetail { get; set; }
        public string JobResponsibilitiesTitle { get; set; }
        public string JobSpecificDetail { get; set; }
        public string JobSpecificTitle { get; set; }
        public string JobTitle { get; set; }
        public string PerformanceRequirementsDetail { get; set; }
        public string PerformanceRequirementsTitle { get; set; }
        public DateTime PostDate { get; set; }
        public string Preferred { get; set; }
        public string Type { get; set; }
        public string WhatWeCanOfferDescription { get; set; }
        public string WhatWeCanOfferTitle { get; set; }

        public string JobDepartment { get; set; }

        public string ChooseProperties { get; set; }



        public bool JobUrgent { get; set; }

        public string Url { get; set; }
        public DateTime PublishDate { get; set; }
    }
}
