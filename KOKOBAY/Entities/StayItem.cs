﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class StayItem
    {
        public ImageItem Image { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string ShortDescription { get; set; }
        public string UrlStay { get; set; }
        public string Link { get; set; }
        public DateTime PublishDate { get; set; }

        public List<ImageItem> ImageSlider { get; set; }
        public List<ImageItem> LstImage { get; set; }
        public string UrlBooking { get; set; }
    }
}