﻿using KOKOBAY.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static KOKOBAY.helper.KOKO_Const;

namespace KOKOBAY.Entities
{
    public class BookingItem
    {

        [Display(Name = "Restaurant Name")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string RestaurantName { get; set; }

        [Display(Name = "Your Email")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string Email { get; set; }

        [Display(Name = "Customer Name")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string CustomerName { get; set; }

        [Display(Name = "Phone Number")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        [RegularExpression(ConstFormatCustom.NUMBER, ErrorMessage = ScreenMessage.E0004)]
        public string PhoneNumber { get; set; }

        [Display(Name = "Special Note")]
        public string SpecialNote { get; set; }

        [Display(Name = "Number Of Seats")]
        [Range(1, 100, ErrorMessage = ScreenMessage.E0006)]
        public int NumberOfSeats { get; set; }

        [Display(Name = "Booking Date")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        //[RegularExpression("^[0-3]?[0-9]-[0-3]?[0-9]-(?:[0-9]{2})?[0-9]{2}$", ErrorMessage = ScreenMessage.E0002)]
        public string BookingDate { get; set; }

        [Display(Name = "Booking Time")]
        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string BookingTime { get; set; }

        public List<SelectListItem> ListNumberOfSeats { get; set; }
        public List<SelectListItem> ListRestaurant { get; set; }
        public List<SelectListItem> ListTime { get; set; }


    }
}