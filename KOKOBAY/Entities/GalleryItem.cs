﻿namespace KOKOBAY.Entities
{
    public class GalleryItem
    {
        public string Title { get; set; }
        public ImageItem Photo { get; set; }
        public string DataWidth { get; set; }
        public string DataHeight { get; set; }
        public LinkItem Link { get; set; }
    }
}