using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class OfferItem
    {

        public bool DisplayMenuTop { get; set; }
        public IEnumerable<IPublishedContent> ImageGalerry { get; set; }

        public ImageItem BannerImage { get; set; }
        public ImageItem IconBannerImage { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }

        public DateTime PublishDate { get; set; }
    }
}
