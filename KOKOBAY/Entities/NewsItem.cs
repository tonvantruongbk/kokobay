﻿using System;
using System.Collections.Generic;

namespace KOKOBAY.Entities
{
    public class NewsItem
    {
        public ImageItem Image { get; set; }
        public string Title { get; set; }
        public string ShortDescription { get; set; }
        public ImageItem ImageContent { get; set; }
        public string DescriptionImageContent { get; set; }
        public string Description { get; set; }
        public DateTime PublishDate { get; set; }
        public string Author { get; set; }
        public List<LinkItem> Tags { get; set; }
        public string UrlNews { get; set; }
        public string OfferType { get; set; }
    }
}