using System;
using System.Collections.Generic;
using System.Web;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class RecruitmentItem
    {

        public string CandidateEmail { get; set; }
        public string CandidateName { get; set; }
        public string CandidatePhoneNumber { get; set; }
        public HttpPostedFileBase CandidateCV { get; set; }

        public string JobTitle { get; set; }
        public DateTime RecruitmentCreateDate { get; set; }

        public DateTime PublishDate { get; set; }
     
    }
}
