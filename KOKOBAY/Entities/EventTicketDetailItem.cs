﻿using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class EventTicketDetailItem
    {

        public string Title { get; set; }
        public ImageItem AvatarImage { get; set; }

        public string SubTitle { get; set; }
        public string ShortDescription { get; set; }
        public string PHONE { get; set; }
        public string HOURSOFOPERATION { get; set; }
        public string AGERESTRICTIONS  {get;set;}

        public string PRICERANGE { get; set; }

        public string VideoTitle { get; set; }

        public string VideoSubTitle { get; set; }

        public string VideoDescription { get; set; }

        public string VideoLink { get; set; }

        public string TitleGallery { get; set; }

        public string SubTitleGallery { get; set; }

        public List<ImageItem> GalleryImage { get; set; }

        public string LocationTitle { get; set; }

        public ImageItem LocationImage { get; set; }

        public string LocationDescription { get; set; }
        public string DETAILEDAGENDA { get; set; }
        public string LinkBookTicket { get; set; }


        public string DetailAgendaTitle { get; set; }
        public string DEtailedagenda { get; set; }
        public string DetailedAgendaSubtitle { get; set; }
        public bool EnableDetailAgenda { get; set; }



        public List<EventTicketDetailColumns> EventTicketDetailColumnsList { get; set; }

    }

    public class EventTicketDetailTimes
    {
        public string EventTicketDetailDescription { get; set; }
        public string EventTicketDetailName { get; set; }
        public DateTime FromTime { get; set; }
        public DateTime ToTime { get; set; }
        public string eventColor { get; set; }
    }

    public class EventTicketDetailColumns
    {
        public string EventColumnName { get; set; }
        public List<EventTicketDetailTimes> EventTicketDetailRowList { get; set; }
        public IEnumerable<IPublishedContent> EventTicketDetailRow { get; set; }
    }
}