using System;
using System.Collections.Generic;
using Umbraco.Core.Models;

namespace KOKOBAY.Entities
{
    public class EventDetailItem
    {

        public string Author { get; set; }
        public ImageItem AvatarImage { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Description { get; set; }
        public bool DisplayHome { get; set; }
        public string ShortDescription { get; set; }
        public IEnumerable<string> Tag { get; set; }
        public string Title { get; set; }
        public string UrlEvent { get; set; }


        public DateTime PublishDate { get; set; }
    }
}
