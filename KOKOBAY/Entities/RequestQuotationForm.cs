﻿using KOKOBAY.helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static KOKOBAY.helper.KOKO_Const;

namespace KOKOBAY.Entities
{
    public class RequestQuotationForm
    {

        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string CompanyName { get; set; }


        [Required(ErrorMessage = ScreenMessage.E0001)]
        public int NumberOfDelegates { get; set; }


        public string PackageRequest { get; set; }

        public string Request { get; set; }


        public string Nationality { get; set; }

        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string EventDate { get; set; }


        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string CheckIn { get; set; }

        [Required(ErrorMessage = ScreenMessage.E0001)]
        public string CheckOut { get; set; }


        public string SetUpStyles { get; set; }

        public string SpecialRequest { get; set; }

        public List<SelectListItem> ListPackageRequest { get; set; }
        public List<SelectListItem> ListRequest { get; set; }

    }
}