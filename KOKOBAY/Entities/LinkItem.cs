﻿namespace KOKOBAY.Entities
{
    public class LinkItem
    {
        public string CssClass { get; set; }

        public string Target { get; set; }

        public string Title { get; set; }

        public string Url { get; set; }

        public string ImageUrl { get; set; }
    }
}