﻿using KOKOBAY.Controller;
using KOKOBAY.Entities;
using KOKOBAY.helper;
using KOKOBAY.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;

namespace KOKOBAY.Controllers
{
    public class LastNewsController : SurfaceController
    {

        public ActionResult GetLastListNews(List<NewsItem> model)
        {
            model = new List<NewsItem>();
            var NewsNode = Umbraco.ContentSingleAtXPath("//news");

            IEnumerable<IPublishedContent> lstNewContent = NewsNode.Children.Take(4);

            foreach (var item in lstNewContent)
            {

                NewsItem ni = new NewsItem();
                var itemIMG = item.GetProperty("avatarImage");
                ni.Image = Helper.GetMediaPicker(((IPublishedContent)(item.GetProperty("avatarImage").Value)).Id.ToString());
                ni.UrlNews = item.Url;
                ni.Title = item.GetProperty("title").Value.ToString();
                ni.ShortDescription = item.GetProperty("shortDescription").Value.ToString();
                ni.Description = ((Umbraco.Web.Models.DynamicPublishedContent)NewsNode).Url;
                model.Add(ni);

            }
            return PartialView("News/LastListNews", model);
        }
    }
}