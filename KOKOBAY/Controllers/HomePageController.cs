﻿using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;

using KOKOBAY.helper;
namespace KOKOBAY.Controller
{
    public class HomePageController : BaseController
    {
        public ActionResult HomePage()
        {
            var model = new HomeViewModel(CurrentPage);
            return CurrentTemplate(model);
        }

    }
}