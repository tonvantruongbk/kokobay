﻿using KOKOBAY.Entities;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using umbraco.NodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
namespace KOKOBAY.Controller
{
    public class ShoppingSurfaceController : SurfaceController
    {

        public ActionResult Index()
        {
            var model = new RequestQuotationForm();
            model.ListPackageRequest = new List<SelectListItem>
            {
            new SelectListItem{ Text="With lunch", Value = "With lunch" },
            new SelectListItem{ Text="Without lunch", Value = "Without lunch" }
            };


            model.ListRequest = new List<SelectListItem>
            {
                new SelectListItem{ Text="Meeting room rental", Value = "Meeting room rental" },
                new SelectListItem{ Text="Half day package", Value = "Half day package" },
                new SelectListItem{ Text="Full day package", Value = "Full day package" },
            };


            return PartialView("~/Views/Partials/Shopping.cshtml", model);
        }

        [HttpPost]
        public string Shopping(RequestQuotationForm model)
        {
            if (ModelState.IsValid)
            {
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var noteShopping = umbracoHelper.ContentSingleAtXPath("//shopping");


                var newShopping = Services.ContentService.CreateContent(model.CompanyName + " MK:" + ((Umbraco.Web.Models.DynamicPublishedContent)noteShopping).Id, ((Umbraco.Web.Models.DynamicPublishedContent)noteShopping).Id, "requestQuotationForm");

                newShopping.SetValue("numberOfDelegates", model.NumberOfDelegates);
                newShopping.SetValue("packageRequest", model.PackageRequest);
                newShopping.SetValue("request", model.Request);
                newShopping.SetValue("Nationality", model.Nationality);
                newShopping.SetValue("EventDate", DateTime.ParseExact(model.EventDate, "yyyy-MM-dd", CultureInfo.InvariantCulture));
                newShopping.SetValue("CheckIn", model.CheckIn);
                newShopping.SetValue("CheckOut", model.CheckOut);
                newShopping.SetValue("SetUpStyles", model.SetUpStyles);
                newShopping.SetValue("SpecialRequest", model.SpecialRequest);

                Services.ContentService.SaveAndPublishWithStatus(newShopping);
                TempData["Status"] = true;

                return "Đã yêu cầu thành công!";
            }
            return "Cập nhật thất bại";
        }

        // validate logic
        private bool CheckTime(string bookDay, string bookTime)
        {
            string temp = bookDay + " " + bookTime;
            DateTime day = Convert.ToDateTime(temp);
            if (day < DateTime.Now)
            {
                return false;
            }
            return true;
        }

        //public void SendMail(string toEmailAddress, string subject, string content)
        //{
        //    var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
        //    var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
        //    var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
        //    var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
        //    var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

        //    bool enabledSsl = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

        //    string body = content;
        //    MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
        //    message.Subject = subject;
        //    message.IsBodyHtml = true;
        //    message.Body = body;

        //    var client = new SmtpClient();
        //    client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
        //    client.Host = smtpHost;
        //    client.EnableSsl = enabledSsl;
        //    client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
        //    client.Send(message);
        //}
    }
}