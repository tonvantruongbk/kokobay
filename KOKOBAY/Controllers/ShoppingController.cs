using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class ShoppingController : BaseController
    {
        public ActionResult Index()
        {
            var model = new ShoppingModel(CurrentPage);
            return View("Shopping/Index", model);
        }
    }
}
