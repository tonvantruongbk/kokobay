using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class ProjectDetailController : BaseController
    {        public ActionResult Index()
        {
            var model = new ProjectDetailModel(CurrentPage);
            return View("Projects/Detail", model);
        }
    }
}
