using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class AllAboutUsController : BaseController
    {
        public ActionResult Index()
        {
            var model = new AboutUsModel(CurrentPage);
            return View("AboutUs/Index", model);
        }
    }
}
