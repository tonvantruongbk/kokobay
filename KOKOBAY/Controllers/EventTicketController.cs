﻿using KOKOBAY.Controller;
using KOKOBAY.Entities;
using KOKOBAY.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace KOKOBAY.Controllers
{
    public class EventTicketController : BaseController
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new EventTicketModel(CurrentPage);
            return View("EventTicket/Index", model);
        }

        public ActionResult GetLastListNews()
        {
            List<NewsItem> model = new List<NewsItem>();

            Umbraco.ContentSingleAtXPath("//MyDocType");

            return PartialView("News/LastListNews", model);
        }
    }
}