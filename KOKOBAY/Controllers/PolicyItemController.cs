﻿using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller.GNews
{
    public class PolicyItemController : BaseController
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new PolicyItemModel(CurrentPage);
            return View("Policy/Detail", model);
        }
    }
}