﻿using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller.GNews
{
    public class NewsLandingController : BaseController
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new NewsLandingModel(CurrentPage);
            return View("News/Detail", model);
        }
    }
}