﻿using KOKOBAY.Controller;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KOKOBAY.Controllers
{
    public class StayController : BaseController
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new StayModel(CurrentPage);
            return View("Stay/Index", model);
        }
    }
}