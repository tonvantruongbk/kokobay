﻿using KOKOBAY.Entities;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using umbraco.NodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
namespace KOKOBAY.Controller
{
    public class BookingSurfaceController : SurfaceController
    {

        public ActionResult Index()
        {
            var model = new BookingItem();
            model.ListNumberOfSeats = new List<SelectListItem>
            {
            new SelectListItem{ Text="1", Value = "1" },
            new SelectListItem{ Text="2", Value = "2" },
            new SelectListItem{ Text="3", Value = "3" },
            new SelectListItem{ Text="4", Value = "4" },
            new SelectListItem{ Text="5", Value = "5" },
            new SelectListItem{ Text="6", Value = "6" },
            new SelectListItem{ Text="7", Value = "7" },
            new SelectListItem{ Text="8", Value = "8" },
            new SelectListItem{ Text="9", Value = "9" },
            new SelectListItem{ Text="10", Value = "10" },
            };

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var Feasts = umbracoHelper.ContentSingleAtXPath("//feasts");
            model.ListRestaurant = new List<SelectListItem>();
            foreach (var item in Feasts.Children)
            {
                model.ListRestaurant.Add(new SelectListItem { Text = item.Name, Value = item.Name });
            }

            model.ListTime = new List<SelectListItem>
            {
                new SelectListItem{ Text="07:00", Value = "07:00" },
                new SelectListItem{ Text="08:00", Value = "08:00" },
                new SelectListItem{ Text="09:00", Value = "09:00" },
                new SelectListItem{ Text="10:00", Value = "10:00" },
                new SelectListItem{ Text="11:00", Value = "11:00" },
                new SelectListItem{ Text="12:00", Value = "12:00" },
                new SelectListItem{ Text="13:00", Value = "13:00" },
                new SelectListItem{ Text="14:00", Value = "14:00" },
                new SelectListItem{ Text="15:00", Value = "15:00" },
                new SelectListItem{ Text="16:00", Value = "16:00" },
                new SelectListItem{ Text="17:00", Value = "17:00" },
                new SelectListItem{ Text="18:00", Value = "18:00" },
                new SelectListItem{ Text="19:00", Value = "19:00" },
                new SelectListItem{ Text="20:00", Value = "20:00" },
                new SelectListItem{ Text="21:00", Value = "21:00" },
                new SelectListItem{ Text="22:00", Value = "22:00" },
                new SelectListItem{ Text="23:00", Value = "23:00" },
                new SelectListItem{ Text="24:00", Value = "24:00" },
            };



            return PartialView("~/Views/Partials/Booking.cshtml", model);
        }

        [HttpPost]
        public string Booking(BookingItem model)
        {
            if (ModelState.IsValid)
            {


                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var noteBooking = umbracoHelper.ContentSingleAtXPath("//booking");


                var rootNodeCoco = umbracoHelper.ContentSingleAtXPath("//kOKOBAYNEW");
                var EnableSendMail = rootNodeCoco.GetPropertyValue<Boolean>("EnableSendMail");
                if (EnableSendMail = true)
                {
                    try
                    {
                        var fromEmailAddress = rootNodeCoco.GetPropertyValue<string>("emailServer");
                        var toEmailAddress = model.Email;
                        var fromEmailDisplayName = rootNodeCoco.GetPropertyValue<string>("emailServer");
                        var smtpHost = rootNodeCoco.GetPropertyValue<string>("hostMail");
                        var fromEmailPassword = rootNodeCoco.GetPropertyValue<string>("password");
                        var enabledSsl = (string)rootNodeCoco.GetPropertyValue<string>("SSL") == "" ? false : true;
                        var smtpPort = rootNodeCoco.GetPropertyValue<string>("port");
                        var bookingTemplate = (string)rootNodeCoco.GetPropertyValue<string>("bookingTemplate");

                        bookingTemplate = bookingTemplate.Replace("<span style=\"font-weight: 400;\">", "");
                        bookingTemplate = bookingTemplate.Replace("</span>", "");
                        bookingTemplate = bookingTemplate.Replace("<span" + Environment.NewLine + "style=\"font-weight: 400;\">", "");
                        bookingTemplate = bookingTemplate.Replace("[Mail subject] Your table reservation [#reservation code]", "");

                        bookingTemplate = bookingTemplate.Replace("[customer name]", model.CustomerName);
                        bookingTemplate = bookingTemplate.Replace("[customer’s phone number] ", model.PhoneNumber);
                        bookingTemplate = bookingTemplate.Replace("[restaurant name]", model.RestaurantName);
                        bookingTemplate = bookingTemplate.Replace("[customer’s note]", model.SpecialNote);
                        bookingTemplate = bookingTemplate.Replace("[number]", model.NumberOfSeats + "");
                        bookingTemplate = bookingTemplate.Replace("day/month/year]", model.BookingDate);
                        bookingTemplate = bookingTemplate.Replace("[hour;", model.BookingTime);
                        bookingTemplate = bookingTemplate.Replace("[code]", ((Umbraco.Web.Models.DynamicPublishedContent)noteBooking).Id + "");


                        string body = bookingTemplate;
                        MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
                        message.Subject = "Thông tin đặt bàn/Your table reservation " + ((Umbraco.Web.Models.DynamicPublishedContent)noteBooking).Id;
                        message.IsBodyHtml = true;
                        message.Body = body;

                        var client = new SmtpClient();
                        client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
                        client.Host = smtpHost;
                        client.EnableSsl = true;
                        client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
                        client.Send(message);

                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                var newBooking = Services.ContentService.CreateContent(model.CustomerName + " MK:" + ((Umbraco.Web.Models.DynamicPublishedContent)noteBooking).Id, ((Umbraco.Web.Models.DynamicPublishedContent)noteBooking).Id, "bookingItem");

                newBooking.SetValue("email", model.Email);
                newBooking.SetValue("customerName", model.CustomerName);
                newBooking.SetValue("phoneNumber", model.PhoneNumber);
                newBooking.SetValue("restaurantName", model.RestaurantName);
                newBooking.SetValue("specialNote", model.SpecialNote);
                newBooking.SetValue("numberOfSeats", model.NumberOfSeats);
                newBooking.SetValue("bookingDate", DateTime.ParseExact(model.BookingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture));
                newBooking.SetValue("bookingTime", model.BookingTime);

                Services.ContentService.SaveAndPublishWithStatus(newBooking);
                TempData["Status"] = true;

                return "Đã đặt bàn thành công!";
            }
            return "Cập nhật thất bại";
        }

        // validate logic
        private bool CheckTime(string bookDay, string bookTime)
        {
            string temp = bookDay + " " + bookTime;
            DateTime day = Convert.ToDateTime(temp);
            if (day < DateTime.Now)
            {
                return false;
            }
            return true;
        }


    }
}