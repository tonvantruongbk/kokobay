using KOKOBAY.Models;
using System.Linq;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class ProjectsController : BaseController
    {
        public ActionResult Index()
        {
            var model = new ProjectDetailModel(CurrentPage.Children.First());
            return View("Projects/Detail", model);
        }
    }
}
