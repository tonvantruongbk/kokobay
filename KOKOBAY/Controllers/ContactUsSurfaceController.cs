﻿using KOKOBAY.Entities;
using KOKOBAY.helper;
using KOKOBAY.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace KOKOBAY.Controller
{

    public class ContactUsSurfaceController : SurfaceController
    {
        [HttpPost]
        public string Contact(ContactUsItem model)
        {
            //var homePage = CurrentPage.AncestorOrSelf(2);
            //var lstNoteSubcribe = homePage.Children.Where(x => x.DocumentTypeAlias.Equals("contact")).FirstOrDefault();

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var lstNoteSubcribe = umbracoHelper.ContentSingleAtXPath("//contact");


            if (lstNoteSubcribe != null)
            {
                if (ModelState.IsValid)
                {
                   
                    var notecontact = umbracoHelper.ContentSingleAtXPath("//contact");
                    var newcontact = Services.ContentService.CreateContent(model.NameCustomer,notecontact.Id, "contactItem");
                    newcontact.SetValue("email", model.Email);
                    newcontact.SetValue("nameCustomer", model.NameCustomer);
                    newcontact.SetValue("phoneNumber", model.PhoneNumber);
                    newcontact.SetValue("note", model.Note);

                    Services.ContentService.SaveAndPublishWithStatus(newcontact);
                   
                    return  "Bạn đã liên hệ thành công. Cảm ơn bạn!";  
                }
            }

            return "Bạn chưa nhập đủ thông tin !";

        }
    }
}