using KOKOBAY.Entities;
using KOKOBAY.helper;
using KOKOBAY.Models;
using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace KOKOBAY.Controller
{
    public class SearchController : BaseController
    {
        private SearchHelper _searchHelper { get { return new SearchHelper(new UmbracoHelper(UmbracoContext.Current)); } }

        public ActionResult Index(string q)
        {
            var search = new SearchViewModel();
            search.DocTypeAliases = "newsLanding";
            search.FieldPropertyAliases = "description";
            search.PageSize = 500;
            search.PagingGroupSize = 1000;

            if (!string.IsNullOrEmpty(q))
            {
                search.SearchTerm = q;
                search.SearchGroups = GetSearchGroups(search);
                search.SearchResults = _searchHelper.GetSearchResults(search, Request.Form.AllKeys);
            }
            var model = new SearchModel(CurrentPage);
            model.SearchResult = search.SearchResults;

            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var NewsNode = umbracoHelper.ContentSingleAtXPath("//news");

            IEnumerable<IPublishedContent> lstNewContent = NewsNode.Children;


            model.lstEvents = new List<NewsItem>();
            foreach (var item in lstNewContent)
            {
                if (item.GetPropertyValue<string>("typeNews") == "Event")
                {
                    NewsItem ni = new NewsItem();
                    var itemIMG = item.GetProperty("avatarImage");
                    ni.Image = Helper.GetMediaPicker(((IPublishedContent)(item.GetProperty("avatarImage").Value)).Id + "");
                    ni.UrlNews = item.Url;
                    ni.Title = item.GetProperty("title").Value + "";
                    ni.ShortDescription = item.GetProperty("shortDescription").Value + "";
                    ni.Description = ((Umbraco.Web.Models.DynamicPublishedContent)NewsNode).Url;
                    model.lstEvents.Add(ni);
                }
            }

            return View("Search/Index", model);
        }


        private List<SearchGroup> GetSearchGroups(SearchViewModel model)
        {
            List<SearchGroup> searchGroups = null;
            if (!string.IsNullOrEmpty(model.FieldPropertyAliases))
            {
                searchGroups = new List<SearchGroup>();
                searchGroups.Add(new SearchGroup(model.FieldPropertyAliases.Split(','), model.SearchTerm.Split(' ')));
            }
            return searchGroups;
        }

        [HttpGet]

        public ActionResult RenderSearchForm(string query, string docTypeAliases, string fieldPropertyAliases, int pageSize, int pagingGroupSize)
        {
            SearchViewModel model = new SearchViewModel();
            if (!string.IsNullOrEmpty(query))
            {
                model.SearchTerm = query;
                model.DocTypeAliases = docTypeAliases;
                model.FieldPropertyAliases = fieldPropertyAliases;
                model.PageSize = pageSize;
                model.PagingGroupSize = pagingGroupSize;
                model.SearchGroups = GetSearchGroups(model);
                model.SearchResults = _searchHelper.GetSearchResults(model, Request.Form.AllKeys);
            }
            return PartialView("Partials/_SearchForm", model);
        }


        [HttpPost]
        public ActionResult SubmitSearchForm(SearchViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(model.SearchTerm))
                {
                    model.SearchTerm = model.SearchTerm;
                    model.SearchGroups = GetSearchGroups(model);
                    model.SearchResults = _searchHelper.GetSearchResults(model, Request.Form.AllKeys);
                }
                return PartialView("Partials/_NewsSearchResults", model.SearchResults);
            }
            return null;
        }

    }
}