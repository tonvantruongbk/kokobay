﻿using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller.GNews
{
    public class EventTicketDetailController : BaseController
    {
        // GET: News
        public ActionResult Index()
        {
            var model = new EventTicketDetailModel(CurrentPage);
            return View("EventTicket/Detail", model);
        }
    }
}