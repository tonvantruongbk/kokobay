﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace KOKOBAY.Controller
{
    public class BaseController : RenderMvcController
    {
        /// <summary>
        /// Aliases of doctypes that should be excluded from navigation etc.
        /// </summary>
        private static readonly string[] ValidLanguages =
        {
            "vi",
            "en",
            "fr"
        };

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!UmbracoContext.Current.IsFrontEndUmbracoRequest) return;
            if (filterContext.HttpContext.Request.IsAjaxRequest()) return;
            if (CurrentPage == null || CurrentPage.TemplateId < 0) return;
            var cultureName = filterContext.HttpContext.Request.Url != null
                && filterContext.HttpContext.Request.Url.Segments.Length > 1
                && !string.IsNullOrEmpty(filterContext.HttpContext.Request.Url.Segments[1])
                ? filterContext.HttpContext.Request.Url.Segments[1].Trim('/').ToLower()
                : ValidLanguages[0];
            if (!ValidLanguages.Contains(cultureName))
                cultureName = ValidLanguages[0];

            // Attempt to read the culture cookie from Request
            var cultureCookie = filterContext.HttpContext.Request.Cookies["_culture"] ?? new HttpCookie("_culture");

            var currentUrl = string.Empty;
            if (filterContext.HttpContext.Request.Url?.Segments.Length > 1
                && !ValidLanguages.Contains(filterContext.HttpContext.Request.Url?.Segments[1].Trim('/').ToLower()))
            {
                currentUrl = filterContext.HttpContext.Request.Url?.AbsolutePath;
                currentUrl = $"/{cultureName}{currentUrl}";
            }
            if (filterContext.HttpContext.Request.Url != null
                && !string.IsNullOrEmpty(cultureCookie.Value)
                && cultureName.Equals(cultureCookie.Value, StringComparison.OrdinalIgnoreCase)
                && filterContext.HttpContext.Request.Url.Segments.Length > 1)
            {
                if (!string.IsNullOrEmpty(currentUrl))
                    filterContext.Result = new RedirectResult(currentUrl);
                return;
            }
            if (filterContext.HttpContext.Request.Url != null
                && (filterContext.HttpContext.Request.Url.Segments.Length < 2 || string.IsNullOrEmpty(filterContext.HttpContext.Request.Url.Segments[1])))
            {
                if (!string.IsNullOrEmpty(cultureCookie.Value))
                    cultureName = cultureCookie.Value;
            }
            cultureCookie.Value = cultureName.ToLower();
            cultureCookie.Expires = DateTime.Now.AddYears(1);
            Response.Cookies.Add(cultureCookie);

            if (filterContext.HttpContext.Request.Url?.Segments.Length < 2 || !ValidLanguages.Contains(filterContext.HttpContext.Request.Url?.Segments[1].Trim('/').ToLower()))
            {
                filterContext.Result = string.IsNullOrEmpty(currentUrl)
                    ? new RedirectResult($"/{cultureName}/")
                    : new RedirectResult(currentUrl);
            }
        }

    }
}