using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class FeastDetailController : BaseController
    {        public ActionResult Index()
        {
            var model = new FeastDetailModel(CurrentPage);
            return View("Feasts/Detail", model);
        }
    }
}
