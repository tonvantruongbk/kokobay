﻿using KOKOBAY.Entities;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using umbraco.NodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
namespace KOKOBAY.Controller
{
    public class requestQuotationFormSurfaceController : SurfaceController
    {

        public ActionResult Index(string id)
        {
            var model = new RecruitmentItem();
            model.JobTitle = id;
            return PartialView("~/Views/Partials/Recruitment.cshtml", model);
        }

        [HttpPost]
        public string Applying(RecruitmentItem model)
        {
            if (ModelState.IsValid)
            {

                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var RecruitmentNode = umbracoHelper.ContentSingleAtXPath("//requestQuotationForm");

                var newRecruitment = Services.ContentService.CreateContent(model.CandidateName + " MK:", ((Umbraco.Web.Models.DynamicPublishedContent)RecruitmentNode).Id, "recruitmentItem");

                newRecruitment.SetValue("candidateName", model.CandidateName);
                newRecruitment.SetValue("candidateEmail", model.CandidateEmail);
                newRecruitment.SetValue("candidatePhoneNumber", model.CandidatePhoneNumber);
                newRecruitment.SetValue("candidateCV", model.CandidateCV);
                newRecruitment.SetValue("jobTitle", model.JobTitle);

                newRecruitment.SetValue("RecruitmentCreateDate", DateTime.Now);

                Services.ContentService.SaveAndPublishWithStatus(newRecruitment);
                TempData["Status"] = true;

                return "Đã ứng tuyển thành công!";
            }
            return "Cập nhật thất bại";
        }

        // validate logic
        private bool CheckTime(string bookDay, string bookTime)
        {
            string temp = bookDay + " " + bookTime;
            DateTime day = Convert.ToDateTime(temp);
            if (day < DateTime.Now)
            {
                return false;
            }
            return true;
        }

        public void SendMail(string toEmailAddress, string subject, string content)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enabledSsl = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enabledSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(message);
        }
    }
}