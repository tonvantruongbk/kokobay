﻿using KOKOBAY.Entities;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using umbraco.NodeFactory;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
namespace KOKOBAY.Controller
{
    public class RecruitmentSurfaceController : SurfaceController
    {

        public ActionResult Index(string id)
        {
            var model = new RecruitmentItem();
            model.JobTitle = id;
            return PartialView("~/Views/Partials/Recruitment.cshtml", model);
        }

        [HttpPost]
        public string Applying(RecruitmentItem model)
        {
            if (ModelState.IsValid)
            {

                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var RecruitmentNode = umbracoHelper.ContentSingleAtXPath("//recruitment");

                var newRecruitment = Services.ContentService.CreateContent(model.CandidateName + " MK:", ((Umbraco.Web.Models.DynamicPublishedContent)RecruitmentNode).Id, "recruitmentItem");

                newRecruitment.SetValue("candidateName", model.CandidateName);
                newRecruitment.SetValue("candidateEmail", model.CandidateEmail);
                newRecruitment.SetValue("candidatePhoneNumber", model.CandidatePhoneNumber);
                newRecruitment.SetValue("candidateCV", model.CandidateCV);
                newRecruitment.SetValue("jobTitle", model.JobTitle);

                newRecruitment.SetValue("RecruitmentCreateDate", DateTime.Now);

                Services.ContentService.SaveAndPublishWithStatus(newRecruitment);

                TempData["Status"] = true;
                var rootNodeCoco = umbracoHelper.ContentSingleAtXPath("//kOKOBAYNEW");
                var EnableSendMail = rootNodeCoco.GetPropertyValue<Boolean>("EnableSendMail");
                if (EnableSendMail = true)
                {
                    try
                    {
                        var fromEmailAddress = rootNodeCoco.GetPropertyValue<string>("emailServer");
                        var toEmailAddress = model.CandidateEmail;
                        var fromEmailDisplayName = rootNodeCoco.GetPropertyValue<string>("emailServer");
                        var smtpHost = rootNodeCoco.GetPropertyValue<string>("hostMail");
                        var fromEmailPassword = rootNodeCoco.GetPropertyValue<string>("password");
                        var enabledSsl = (string)rootNodeCoco.GetPropertyValue<string>("SSL") == "" ? false : true;
                        var smtpPort = rootNodeCoco.GetPropertyValue<string>("port");
                        var HRTemplate = (string)rootNodeCoco.GetPropertyValue<string>("HRTemplate");

                        HRTemplate = HRTemplate.Replace("<span style=\"font-weight: 400;\">", "");
                        HRTemplate = HRTemplate.Replace("</span>", "");
                        HRTemplate = HRTemplate.Replace("<span" + Environment.NewLine + "style=\"font-weight: 400;\">", "");
                        HRTemplate = HRTemplate.Replace("[Mail subject] Thank you for your application to Cocobay.", "");

                        HRTemplate = HRTemplate.Replace("[#candidate name]", model.CandidateName);


                        string body = HRTemplate;
                        MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
                        message.Subject = "Thank you for your application to Cocobay.";
                        message.IsBodyHtml = true;
                        message.Body = body;

                        var client = new SmtpClient();
                        client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
                        client.Host = smtpHost;
                        client.EnableSsl = true;
                        client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
                        client.Send(message);


                        var HRAdminTemplate = (string)rootNodeCoco.GetPropertyValue<string>("HRAdminTemplate");

                        HRAdminTemplate = HRAdminTemplate.Replace("<span style=\"font-weight: 400;\">", "");
                        HRAdminTemplate = HRAdminTemplate.Replace("</span>", "");
                        HRAdminTemplate = HRAdminTemplate.Replace("<span" + Environment.NewLine + "style=\"font-weight: 400;\">", "");
                        HRAdminTemplate = HRAdminTemplate.Replace("[Mail subject] [#candidate name] apply for [#Job title]", "");

                        HRAdminTemplate = HRAdminTemplate.Replace("[#candidate name]", model.CandidateName);
                        HRAdminTemplate = HRAdminTemplate.Replace("[#Job title]", model.JobTitle);
                        HRAdminTemplate = HRAdminTemplate.Replace("[candidate’s phone number]", model.CandidatePhoneNumber);
                        HRAdminTemplate = HRAdminTemplate.Replace("[candidate’s email]", model.CandidateEmail);

                        string body2 = HRAdminTemplate;
                        //stars@cocobay.vn
                        MailMessage message2 = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress("stars@cocobay.vn"));
                        message2.Subject = model.CandidateName + " apply for " + model.JobTitle;
                        message2.IsBodyHtml = true;
                        message2.Body = body2;

                        var attachment = new Attachment(Server.MapPath(newRecruitment.Properties[3].Value.ToString()));

                        
                        message2.Attachments.Add(attachment);

                        var client2 = new SmtpClient();
                        client2.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
                        client2.Host = smtpHost;
                        client2.EnableSsl = true;
                        client2.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
                        client2.Send(message2);



                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                return "Đã ứng tuyển thành công!";
            }
            return "Cập nhật thất bại";
        }

        // validate logic
        private bool CheckTime(string bookDay, string bookTime)
        {
            string temp = bookDay + " " + bookTime;
            DateTime day = Convert.ToDateTime(temp);
            if (day < DateTime.Now)
            {
                return false;
            }
            return true;
        }

        public void SendMail(string toEmailAddress, string subject, string content)
        {
            var fromEmailAddress = ConfigurationManager.AppSettings["FromEmailAddress"].ToString();
            var fromEmailDisplayName = ConfigurationManager.AppSettings["FromEmailDisplayName"].ToString();
            var fromEmailPassword = ConfigurationManager.AppSettings["FromEmailPassword"].ToString();
            var smtpHost = ConfigurationManager.AppSettings["SMTPHost"].ToString();
            var smtpPort = ConfigurationManager.AppSettings["SMTPPort"].ToString();

            bool enabledSsl = bool.Parse(ConfigurationManager.AppSettings["EnabledSSL"].ToString());

            string body = content;
            MailMessage message = new MailMessage(new MailAddress(fromEmailAddress, fromEmailDisplayName), new MailAddress(toEmailAddress));
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.Body = body;

            var client = new SmtpClient();
            client.Credentials = new NetworkCredential(fromEmailAddress, fromEmailPassword);
            client.Host = smtpHost;
            client.EnableSsl = enabledSsl;
            client.Port = !string.IsNullOrEmpty(smtpPort) ? Convert.ToInt32(smtpPort) : 0;
            client.Send(message);
        }
    }
}