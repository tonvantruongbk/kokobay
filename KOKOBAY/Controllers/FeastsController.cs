using KOKOBAY.Models;
using System.Linq;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class FeastsController : BaseController
    {
        public ActionResult Index()
        {
            var model = new FeastModel(CurrentPage);
            return View("Feasts/Index", model);
        }
    }
}
