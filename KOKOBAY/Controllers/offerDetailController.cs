﻿using KOKOBAY.Controller;
using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KOKOBAY.Controllers
{
    public class offerDetailController : BaseController
    {
        // GET: playDetail
        public ActionResult Index()
        {
            var model = new OfferDetailModel(CurrentPage);
            return View("Offer/Detail", model);
        }
    }
}