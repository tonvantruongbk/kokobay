using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class ContactController : BaseController
    {
        public ActionResult Index()
        {
            var model = new ContactModel(CurrentPage);
            return View("Contact/Index", model);
        }
    }
}
