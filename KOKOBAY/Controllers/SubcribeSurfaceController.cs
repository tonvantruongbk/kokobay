﻿using KOKOBAY.Entities;
using KOKOBAY.helper;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace KOKOBAY.Controller
{

    public class SubcribeSurfaceController : SurfaceController
    {
        public ActionResult Index()
        {
            var model = new SubcribeItem();
            return PartialView("~/Views/Partials/Subcribe.cshtml", model);
        }

        [HttpPost]
        public string Subcribe(SubcribeItem model)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var noteSubcribe = umbracoHelper.ContentSingleAtXPath("//subcribe");

            var rootNode = new umbraco.NodeFactory.Node(-1);

            if (noteSubcribe != null)
            {
                if (ModelState.IsValid)
                {
                    if (CheckEmail(model.Email))
                    {
                      
                        var newSubcribe = Services.ContentService.CreateContent(model.Email, ((Umbraco.Web.Models.DynamicPublishedContent)noteSubcribe).Id, "subcribeItem");
                        Services.ContentService.SaveAndPublishWithStatus(newSubcribe);
                        return "Bạn đã đăng ký nhận thông tin từ KOKOBAY thành công. Cảm ơn bạn!";
                    }
                    return "Email đã được đăng kí!";
                }
            }
            return "Email nhập không đúng định dạng!";
        }

        public bool CheckEmail(string email)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
            var lstNoteSubcribe = umbracoHelper.ContentSingleAtXPath("//subcribe");

            foreach (var item in lstNoteSubcribe.Children)
            {
                if (item.Name == email)
                {
                    return false;
                }
            }
            return true;


        }
    }
}