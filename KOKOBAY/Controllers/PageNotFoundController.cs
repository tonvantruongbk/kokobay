﻿using KOKOBAY.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{
    public class PageNotFoundController : BaseController
    {
        // GET: Register
        public ActionResult Index()
        {
            var model = new BaseViewModel(CurrentPage);
            return View(model);
        }
    }
}