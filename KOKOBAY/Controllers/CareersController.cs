using KOKOBAY.helper;
using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class CareerController : BaseController
    {
        public ActionResult Index()
        {
            var model = new CareerModel(CurrentPage);
            return View("Career/Index", model);
        }
      
    }
}
