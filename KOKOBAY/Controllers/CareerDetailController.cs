using KOKOBAY.helper;
using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class CareerDetailController : BaseController
    {        public ActionResult Index()
        {
            var model = new CareerDetailModel(CurrentPage);
            return View("Career/Detail", model);
        }
    }
}
