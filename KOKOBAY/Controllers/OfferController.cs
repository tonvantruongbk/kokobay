using KOKOBAY.Models;
using System.Web.Mvc;

namespace KOKOBAY.Controller
{    public class OfferController : BaseController
    {
        public ActionResult Index()
        {
            var model = new OfferModel(CurrentPage);
            return View("Offer/Index", model);
        }
    }
}
